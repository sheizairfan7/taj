package Listeners;

import java.io.IOException;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import tests.BaseClass;
import utilities.ScreenShot;
/**
 * Created by Shanza Irfan Qureshi
 */
public class TestListeners extends BaseClass  implements ITestListener 
{
	public  ExtentTest test;
	
	public void onTestStart(ITestResult result) {					
		// TODO Auto-generated method stub				
	
	}
		
  	
	public void onTestFailure(ITestResult result) {					
		

		if(result.getStatus() == ITestResult.FAILURE){
			//MarkupHelper is used to display the output in different colors
			String screenshotPath;
			try {

				test = extent.createTest(result.getName());
				test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
				test.log(Status.FAIL, MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
				//To capture screenshot path and store the path of the screenshot in the string "screenshotPath"
				//We do pass the path captured by this method in to the extent reports using "logger.addScreenCapture" method. 
				//String Scrnshot=TakeScreenshot.captuerScreenshot(driver,"TestCaseFailed");
               screenshotPath = ScreenShot.getScreenShot(driver, result.getName());
				test.fail("Test Case Failed Snapshot is below " + test.addScreenCaptureFromPath(screenshotPath));		

			} 
			
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//To add it in the extent report 
		}	  		

	}
//	public void onTestSkipped(ITestResult result) {	
//
//		if(result.getStatus() == ITestResult.SKIP)
//			test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));   				
//	}
//
//		
//
//	public void onTestSuccess(ITestResult result) {					
//		if(result.getStatus() == ITestResult.SUCCESS)
//			test.log(Status.PASS, MarkupHelper.createLabel(" Test Case PASSED", ExtentColor.GREEN));			  		
//	}
}	
