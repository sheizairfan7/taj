package tests;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
/**
 * Created by Shanza Irfan Qureshi
 */
public class BaseClass extends ExtentReport {

	public static AppiumDriver<AndroidElement> driver;

	@BeforeTest(alwaysRun = true)
	public void setup()  throws MalformedURLException{

		DesiredCapabilities capabilities= new DesiredCapabilities();

		capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.taj.patient.debug");
		capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.taj.patient.view.activity.SplashActivity");
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
		capabilities.setCapability("PLATFORM_NAME", "Andriod");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "SAMSUNG J7 MAX");
		//capabilities.setCapability(MobileCapabilityType.UDID, udid);
		capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS,true);
		//capabilities.setCapability(AndroidMobileCapabilityType.SYSTEM_PORT, systemPort);

		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AndroidDriver<AndroidElement>(url,capabilities);

		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

	}

	@AfterTest()
	public void tearDown(){
		driver.quit();

	}
}
