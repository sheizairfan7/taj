package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import utilities.ActionMethods;

/**
 * Created by Javaria Naeem
 */


public class PatientRelativeFlow extends BaseClass{
	ExtentTest test;
	Point p;
	WebDriverWait wait;



	@Test(priority=0,enabled=false)
	public void clickOnMenuIcon() throws InterruptedException{
		test =extent.createTest("CLICK MENU ICON","This Test case will click on Menu Icon to select medical profile From Home Screen.");
		test.log(Status.INFO,"Test case Started");	

		driver.findElement(By.id("iv_logo_menu")).click();	
		test.log(Status.INFO, "clicked Menu Icon on Home Screen");
	
	}
	
	@Test(priority=1 , enabled=true)
	public void clickonfamilymember() throws InterruptedException 
	{
		test = extent.createTest("ClICK ON FAMILY MEMBERS","This test case will click on family members from menu");
		test.log(Status.INFO, "Test case Started");
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Family Members']")));
		MobileElement e1 =driver.findElement(By.xpath("//android.widget.TextView[@text='Family Members']"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Family member button clicked");
		Thread.sleep(3000);
	}

	@Test(priority=2 ,enabled=true)
	public void addrelative() throws InterruptedException 
	{
		test= extent.createTest("ADD FAMILY MEMBER","This test case will add family member");
		test.log(Status.INFO, "Test case Started");
		String  title=driver.findElement(By.xpath("//android.widget.TextView[@text='Add Family']")).getText();
		SoftAssert softassert = new SoftAssert();
		softassert.assertEquals(title, title.equals("Add Family")||title.equals("ADD FAMILY"));
		try
		{
			test = extent.createTest("Test Case to add Relative");
			test.log(Status.INFO, "Test case Started");
			wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_father")));
			driver.findElement(By.id("iv_father")).click();
			test.log(Status.PASS, "Father icon is clicked");
			driver.findElement(By.id("et_national_id")).sendKeys("2171615020");
			test.log(Status.PASS, "national id is entered");
			driver.findElement(By.id("et_mobile_no")).sendKeys("789789789");
			test.log(Status.PASS, "mobile number is entered");
			driver.findElement(By.id("view_save_relative")).click();
			test.log(Status.PASS, "save button is clicked");

			VerifyDetails();

			Thread.sleep(3000);
		}
		catch(Exception message)
		{
			test.log(Status.FAIL,"test failed" );
		}


	}
	@Test(priority=3 ,enabled=true)
	public void addrelative1() throws InterruptedException 
	{
		test = extent.createTest("ADD FAMILY MEMBER","This test case will add family member");
		String  title=driver.findElement(By.xpath("//android.widget.TextView[@text='Family Members']")).getText();
		SoftAssert softassert = new SoftAssert();
		softassert.assertEquals(title, title.equals("Family Members")||title.equals("FAMILY MEMBERS"));
		try
		{
			test = extent.createTest("Test Case to add Relative flow2");
			test.log(Status.INFO, "Test case Started");
			wait = new WebDriverWait(driver,10);

			driver.findElement(By.id("tv_add_now2")).click();
			test.log(Status.PASS, "Add button is clicked");
			driver.findElement(By.id("iv_father")).click();
			test.log(Status.PASS, "Father icon is clicked");
			driver.findElement(By.id("et_national_id")).sendKeys("1084409356");
			test.log(Status.PASS, "national id is entered");
			driver.findElement(By.id("et_mobile_no")).sendKeys("45556575");
			test.log(Status.PASS, "mobile number is entered");
			driver.findElement(By.id("view_save_relative")).click();
			test.log(Status.PASS, "save button is clicked");
			Thread.sleep(3000);

		}
		catch(Exception message )
		{
			test.log(Status.FAIL, "test failed");
		}	


	}

	@Test(priority=4 ,enabled=true)
	public void VerifyDetails()
	{


		MobileElement e1=driver.findElement(By.id("btn_next"));
		SoftAssert softassert = new SoftAssert();
		softassert.assertTrue(e1.isDisplayed());
		try
		{
			test= extent.createTest("VERIFY DETAILS","This test case will verify details");
			test.log(Status.INFO, "Test case Started");
			e1.click();
			test.log(Status.PASS, "details verified");
			//		no back
		}

		catch(Exception e )
		{
			test.log(Status.FAIL, "test failed Patient already exist");

		}

	}

}
