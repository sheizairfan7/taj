package tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class MenuElement extends BaseClass {
	public ExtentTest test;
	@Test(priority=1,enabled=false)
	public void clickOnMenuIcon() throws InterruptedException{
		test =extent.createTest("CLICK MENU ICON","This Test case will click on Menu Icon to select medical profile From Home Screen.");
		test.log(Status.INFO,"Test case Started");	

		driver.findElement(By.id("iv_logo_menu")).click();	
		test.log(Status.INFO, "clicked Menu Icon on Home Screen");
	Thread.sleep(5000);
	}

}
