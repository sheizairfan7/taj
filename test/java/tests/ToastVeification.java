package tests;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class ToastVeification extends BaseClass {


	public static String  destDir;
	public static DateFormat dateFormat;
	public static String  scrFile;
	public static String  scrPath;

	public void screenshot()
	{
		//set folder name to store screenshots
		destDir = "screenshots";
		//capture screenshot
		File scrFile=((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		//set date format to set it as file name
		dateFormat= new SimpleDateFormat("dd-MM-yy__hh_mm_ssaa");
		//create folder under project with name "screenshot"  provided to destDir
		new File(destDir).mkdirs();
		//set file name using current date and time
		String destFile=dateFormat.format(new Date(0)) + ".png";
		try
		{
			//copy paste file at destination folder location
			FileUtils.copyFile(scrFile, new File(destDir +"/"+destFile));
			scrPath=destDir+"/" + destFile;
		}
		
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public static String OCR (String ImagePath)
	{
		String result=null;
		File imageFile= new File (ImagePath);
		ITesseract instance = new Tesseract();
		try
		{
			result=instance.doOCR(imageFile);
		}
		catch (TesseractException e)
		{
			System.err.println(e.getMessage());
		}
		return result;
	}
}
