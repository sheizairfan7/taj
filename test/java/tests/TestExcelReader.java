//package tests;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.annotations.Test;
//
//import com.aventstack.extentreports.ExtentTest;
//import com.aventstack.extentreports.Status;
//
//public class TestExcelReader extends BaseClass {
//
// public ExtentTest test ;
//	@Test(priority=0)
//	public void appLanguageScreen() {
//
//		test =extent.createTest("SELECT APPLICATION LANGUAGE","This test case will load language screen and select language");
//		test.log(Status.INFO,"Test case Started");	
//
//		WebDriverWait wait= new WebDriverWait(driver,10);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_eng")));	
//
//		driver.findElement(By.id("iv_eng")).click();
//		test.log(Status.PASS, "English Language is selected");
//
//		driver.findElement(By.id("linearLayout3")).click();
//		test.log(Status.PASS, "Continue Button is clicked");	
//
//	}
//
//	@Test(priority=1)
//	public void enterMobileNumber() {
//
//		test =extent.createTest("ENTER MOBILE NUMBER","This test case will Enter Mobile Number");
//		test.log(Status.INFO,"Test case Started");	
//
//		WebDriverWait wait= new WebDriverWait(driver,10);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("et_mobile_num_")));	
//
//		driver.findElement(By.id("et_mobile_num_")).sendKeys("89988998");
//		test.log(Status.PASS, "Mobile Number is entered");
//
//		driver.findElement(By.id("linearLayout3")).click();
//
//		//reg.clickelemet("SearchPage","searchButton");
//
//		test.log(Status.PASS, "Continue Button is clicked");	
//
//	}
//
//}
