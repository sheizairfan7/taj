package tests;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utilities.ActionMethods;

/**
 * Created by Javaria Naeem
 */

public class HealthscoreHeart extends ToastVeification {
	ExtentTest test;
	Point p;
	WebDriverWait wait;
	//http://code2test.com/appium-tutorial/vertical-and-horizontal-swipe-scroll-in-appium/
	//where x co-ordinates changes but y co-ordinates remains the same.
	
	@Test(priority=1)
	void lifestylescore() throws InterruptedException
	{
		test = extent.createTest("ClICK ON LIFESTYLESCORE","This test case will click on lifestyle score");
		test.log(Status.INFO, "Test Case Started");
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("health_score")));
		driver.findElement(By.id("health_score")).click();
		test.log(Status.PASS, "Heathscore button is clicked");	
		Thread.sleep(5000);	

	}
	@Test(priority=2 , enabled=true)
	void scrollscreen() throws InterruptedException 
	{
		test = extent.createTest("HORIZONTALLY SCROLL SCREEN","This test case will scroll screen horizontally from right to left");
		test.log(Status.INFO, "Started");
		MobileElement thirdElement=driver.findElement(By.id("fitnes_box"));	
		MobileElement firdelement= driver.findElement(By.id("mind_box"));
		ActionMethods.horizontalscroll( thirdElement, firdelement);
		test.log(Status.PASS, "Screen is scrolled");	
		Thread.sleep(5000);	
	}
	//heart

	@Test(priority=3, enabled=true)

	void lifestylescoreheart()
	{
		test = extent.createTest("CLICK ON HEART BUTTON","This test case will click on heart button");
		test.log(Status.INFO, "Test Case Started");
		driver.findElement(By.id("heart_box")).click();	
		test.log(Status.PASS, "Heart button is clicked");
	}
	@Test(priority=4, enabled=true)
	void addHeartdata() throws InterruptedException
	{
		test = extent.createTest("ADD HEART SCORE","This test case will add upper and lower BP");
		test.log(Status.INFO, "Test Case Started");
		MobileElement e1= driver.findElement(By.id("pluse_add"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Add "+" button clicked successfully");
		MobileElement e2= driver.findElement(By.id("wheel_systolic"));
		ActionMethods.verticallyDown(e2,150);
		test.log(Status.PASS, "Systolic BP list is scrolled ");
		Thread.sleep(2000);
		MobileElement e3= driver.findElement(By.id("wheel_dystolic"));
		ActionMethods.verticallyDown(e3,150);
		test.log(Status.PASS, "Dystolic BP list is scrolled");
		Thread.sleep(5000);
		driver.findElement(By.id("bt_calculate")).click();	
		test.log(Status.PASS, "Heart Score is added");
	}
	//Test Scenario 3: Verify that user can reset HealthScore_Shape data
	@Test(priority=5, enabled=true)
	void clickondeletebutton()
	{   test = extent.createTest("CLICK ON DELETE HEART SCORE BUTTON","This test case will click on delete button");
	test.log(Status.INFO, "Test Case Started");
	driver.findElement(By.id("iv_delete_records")).click();
	test.log(Status.PASS, "Delete button is clicked");
	}
	@Test(priority=6, enabled=true)
	void resetheartscore() throws InterruptedException
	{
		test = extent.createTest("RESET HEART SCORE","This test case will reset heart score");
		test.log(Status.INFO, "Started");
		driver.findElement(By.id("btn_delete_all")).click();
		test.log(Status.PASS, "Reset All button is clicked");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		test.log(Status.PASS, "heart is score deleted");	
		Thread.sleep(3000);
	}
	@Test(priority=7, enabled=true)
	void checkalreadydeletedheartdata() throws InterruptedException
	{
		test = extent.createTest("RESET HEART SCORE AGAIN","This test case will try to reset mind score again");
		test.log(Status.INFO, "Started");
		clickondeletebutton();
		resetheartscore();
		test.log(Status.INFO, "Screen shot taken to verify toast if data is not present");
		screenshot();
		test.log(Status.PASS, "Screenshot is taken ");
		String Text= OCR(scrPath);
		System.out.println(Text);
		SoftAssert softassert = new SoftAssert();
		softassert.assertEquals(Text.contains("\"Heart history is not available, Please add your mind data first.\";"), Text.equals("\"Heart history is not available, Please add your mind data first.\";"));
		try {
			test.log(Status.PASS, "Toast Verified . there is no data please add data and then tries to delete");
			WebDriverWait wait=new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_menu_back")));
			driver.findElement(By.id("iv_menu_back")).click();
			Thread.sleep(5000);
		}
		catch(AssertionError e)
		{
			test.log(Status.FAIL, e);
		}


	}
	//Test Scenario 5 : Verify that user can delete shape data individually


	@Test(priority=8 , enabled=true)
	void deleteHeartscore() throws InterruptedException
	{
		addHeartdata();
		test = extent.createTest("DELETE HEART SCORE INDIVIDUALLY","This test case will delete heart score individually");
		test.log(Status.INFO, "Test Case Started");
		driver.findElement(By.id("iv_delete_records")).click();
		driver.findElement(By.id("img_delete_solid")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		Thread.sleep(5000);
		test.log(Status.PASS, "heart score is deleted");	
		driver.findElement(By.id("iv_menu_back")).click();
		Thread.sleep(3000);
	}
	@Test(priority=9 , enabled=true)
	void homescreen() throws InterruptedException
	{
		test = extent.createTest("BACK TO HOME SCREEN","This test case will navigate to home screen");
		driver.findElement(By.id("iv_home_logo")).click();
		Thread.sleep(5000);
	}
}
