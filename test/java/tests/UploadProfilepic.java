package tests;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

/**
 * Created by Javaria Naeem
 */


public class UploadProfilepic extends BaseClass {

	ExtentTest test;
	Point p;
	WebDriverWait wait;

	@Test(priority=1)
	public void uploadpic() throws InterruptedException
	//only for samsung devices
	{
		test = extent.createTest("UPLOAD PROFILE PICTURE","This test case will upload profile picture");
		test.log(Status.INFO, "Started");

		//Click on profile picture
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("riv_home_photo")));
		MobileElement e1 =driver.findElement(By.id("riv_home_photo"));
		e1.click();
		test.log(Status.PASS, "profile pic icon clicked successfully");	
		MobileElement e2 =driver.findElement(By.id("iv_camera"));
		e2.click();
		test.log(Status.PASS, "camera icon clicked successfully");	

		test.log(Status.PASS, "picture capture successfully");	
	
		MobileElement e3=driver.findElement(By.id("com.sec.android.app.camera:id/okay"));
		e3.click();
		test.log(Status.PASS, "successfully uploaded picture from camera");	
		Thread.sleep(5000);
	}

}
