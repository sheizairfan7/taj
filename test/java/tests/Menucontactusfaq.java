package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import utilities.ActionMethods;

/**
 * Created by Javaria Naeem
 */


public class Menucontactusfaq extends BaseClass {

	
	ExtentTest test;
	Point p;
	WebDriverWait wait;
	
	@Test(priority=1 ,enabled=true)
	public void testFAQs() throws InterruptedException 
	{
		test = extent.createTest("VIEW FAQS","This test case will open faqs");
		test.log(Status.INFO, "Test case Started");
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='FAQs']")));
		MobileElement e1 =driver.findElement(By.xpath("//android.widget.TextView[@text='FAQs']"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "FAQ's button clicked");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Who are your practitioners?']")));
		MobileElement e2 =driver.findElement(By.xpath("//android.widget.TextView[@text='Who are your practitioners?']"));
		ActionMethods.tapOnElement(e2);
		test.log(Status.PASS, "first question opened");
		Thread.sleep(3000);
		driver.findElement(By.id("iv_menu_back")).click();
		
	}


	@Test(priority=2 , enabled=true)
	public void contactus() throws InterruptedException
	{

		test = extent.createTest("SUBMIT CONTACT US FORM","This test case will submit contact us form");
		test.log(Status.INFO, "Test case Started");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Contact Us']")).click();
		test.log(Status.PASS, "Contactus button clicked");
		driver.findElement(By.id("et_issue_comments")).sendKeys("abc");
		test.log(Status.PASS, "Comments entered");
		driver.findElement(By.id("et_email_address")).sendKeys("abc@xyz.com");
		test.log(Status.PASS, "email address entered");
		driver.findElement(By.id("bt_send_issue")).click();
		test.log(Status.PASS, "send button clicked");
		Thread.sleep(100000);
	}

}
