package tests;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
/**
 * Created by Shanza Irfan Qureshi
 */
public class ExtentReport {

	public ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	

	@BeforeSuite
	public void reportSetup() {

		htmlReporter = new ExtentHtmlReporter("TajExtentReport.html");
		htmlReporter.config().setEncoding("utf-8");
		htmlReporter.config().setDocumentTitle("Taj Patient Reports");
		htmlReporter.config().setReportName("Automation Test Results");
		htmlReporter.config().setTheme(Theme.STANDARD);

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		extent.setSystemInfo("Automation Tester","Shanza Irfan Qureshi");
		extent.setSystemInfo("OS", "Windows 10");
		extent.setSystemInfo("Environment", "Stagging");
		extent.setSystemInfo("Build No", "TajPatient_20-Sep-2020");	

	}

	@AfterSuite
	public void reportTearDown() {
		extent.flush();		
	}
}