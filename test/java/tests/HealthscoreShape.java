package tests;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utilities.ActionMethods;

/**
 * Created by Javaria Naeem
 */


public class HealthscoreShape extends ToastVeification {
	ExtentTest test;
	Point p;
	WebDriverWait wait;


	@Test(priority=1)
	void lifestylescore() throws InterruptedException
	{
		test = extent.createTest("ClICK ON LIFESTYLESCORE","This test case will click on lifestyle score");
		test.log(Status.INFO, "Test Case Started");
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("health_score")));
		driver.findElement(By.id("health_score")).click();
		test.log(Status.PASS, "Heathscore button is clicked");	
		Thread.sleep(5000);	

	}
	@Test(priority=2)
	void shapeclick()
	{
		test = extent.createTest("ClICK ON SHAPE BUTTON","This test case will click on shape button");
		test.log(Status.INFO, "Started");
		driver.findElement(By.id("shape_box")).click();	
		test.log(Status.PASS, "Shape button is  clicked");
	}
	@Test(priority=3, enabled=true)
	void addshapedata() throws InterruptedException
	{
		//bmi_add
		test = extent.createTest("ADD SHAPE DATA","This test case will add shape data");
		test.log(Status.INFO, "Started");
		MobileElement e1= driver.findElement(By.id("bmi_add"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Add "+" button is clicked");
		MobileElement e2= driver.findElement(By.id("wheel_weight"));
		ActionMethods.verticallyDown(e2, 150);
		test.log(Status.PASS, "weight list is scrolled");
		Thread.sleep(5000);

		MobileElement e3= driver.findElement(By.id("wheel_height"));
		ActionMethods.verticallyDown(e3, 150);
		test.log(Status.PASS, "weight list is scrolled");
		Thread.sleep(5000);

		driver.findElement(By.id("bt_calculate")).click();	
	}
	//Test Scenario 3: Verify that user can reset HealthScore_Shape data
	@Test(priority=4, enabled=true)
	void deleteshapescore()
	{   test = extent.createTest("CLICK ON DELETE BUTTON","This test case will click on delete button");
	test.log(Status.INFO, "Test Case Started");
	driver.findElement(By.id("iv_delete_records")).click();
	test.log(Status.PASS, "Delete button is clicked");
	}
	@Test(priority=5, enabled=true)
	void resetshapescore() throws InterruptedException
	{
		test = extent.createTest("RESET SHAPE SCORE","This test case will reset shape score");
		test.log(Status.INFO, "Test Case Started");
		driver.findElement(By.id("btn_delete_all")).click();
		test.log(Status.PASS, "Reset All button is clicked");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		test.log(Status.PASS, "Shape score is deleted");	
		Thread.sleep(3000);
	}
	@Test(priority=6, enabled=true)
	void checkalreadydeletedshapedata() throws InterruptedException
	{
		test = extent.createTest("RESET SHAPE SCORE AGAIN","This test case will reset shape score again");
		test.log(Status.INFO, "Started");
		deleteshapescore();
		resetshapescore();
		test.log(Status.INFO, "Screen shot taken to verify toast if data is not present");
		screenshot();
		test.log(Status.PASS, "Screenshot taken successfully");
		String Text= OCR(scrPath);
		System.out.println(Text);
		SoftAssert softassert = new SoftAssert();
		softassert.assertEquals(Text.contains("\"Shape history is not available, Please add your shape data first.\";"), Text.equals("\"Shape history is not available, Please add your shape data first.\";"));
		try {
			test.log(Status.PASS, "Toast Verified . there is no data please add data and then tries to delete");
			WebDriverWait wait=new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_menu_back")));
			driver.findElement(By.id("iv_menu_back")).click();
			Thread.sleep(5000);
		}
		catch(AssertionError e)
		{
			test.log(Status.FAIL, e);
		}


	}
	//Test Scenario 5 : Verify that user can delete shape data individually


	@Test(priority=7 , enabled=true)
	void deleteShapescoreindividually() throws InterruptedException
	{
		addshapedata();
		test = extent.createTest("DELETE SHAPE SCORE","This test case will delete shape score ");
		test.log(Status.INFO, "Started");
		driver.findElement(By.id("iv_delete_records")).click();
		driver.findElement(By.id("img_delete_solid")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		Thread.sleep(5000);
		//driver.findElement(By.id("btn_done")).click();
		test.log(Status.PASS, "Shape score is deleted");	
		driver.findElement(By.id("iv_menu_back")).click();
		Thread.sleep(3000);
	}
	@Test(priority=8 , enabled=true)
	void homescreen() throws InterruptedException
	{
		test = extent.createTest("BACK TO HOME SCREEN","This test case will navigate to home screen");
		driver.findElement(By.id("iv_home_logo")).click();
		Thread.sleep(5000);
	}
}
