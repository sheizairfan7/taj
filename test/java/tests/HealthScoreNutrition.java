package tests;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utilities.ActionMethods;

/**
 * Created by Javaria Naeem
 */

public class HealthScoreNutrition extends ToastVeification {

	ExtentTest test;
	Point p;
	WebDriverWait wait;

	
	void add_subtractdata()
	{
		
		for(int i=0;i<=5;i++)
		{
			driver.findElement(By.id("tv_add_item")).click();
		}
		test.log(Status.PASS, "data increased successfully");
		for(int i=0;i<3;i++)
		{
			driver.findElement(By.id("tv_subtract_item")).click();
		}
		test.log(Status.PASS, "data subtracted successfully");

	}
	void servingguide() throws InterruptedException
	{
		
		MobileElement e1=driver.findElement(By.id("tv_serving_type_titles"));
        ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Serving guide is opened");
		Thread.sleep(3000);
		MobileElement e2=driver.findElement(By.id("tv_serving_type_title"));
		 ActionMethods.tapOnElement(e2);
		test.log(Status.PASS, "Serving guide is closed");		
	}
	void savedata() throws InterruptedException
	{

		driver.findElement(By.id("iv_tick")).click();
		test.log(Status.PASS, "data is saved");	
		Thread.sleep(2000);
	}
	@Test(priority=1)
	void healthscore() throws InterruptedException
	{
		test = extent.createTest("ClICK ON LIFESTYLESCORE","This test case will click on lifestyle score");
		test.log(Status.INFO, "Started");
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("arc_progress")));
		driver.findElement(By.id("arc_progress")).click();
		test.log(Status.PASS, "Heathscore button clicked successfully");	
		Thread.sleep(5000);	

	}
	@Test(priority=2 , enabled=true)
	void scrollscreen() throws InterruptedException 
	{
		test = extent.createTest("SCROLL SCREEN TILL NUTRITION","This test case will horizontally scroll screen");
		test.log(Status.INFO, "Test Case Started");
		MobileElement e1=driver.findElement(By.id("fitnes_box"));	
		MobileElement e2= driver.findElement(By.id("mind_box"));
		ActionMethods.horizontalscroll(e1, e2);
		test.log(Status.PASS, "Screen is scrolled");	
		Thread.sleep(5000);	
	}
	@Test(priority=3 , enabled=true)
	void nutritionclick() throws InterruptedException
	{
		test = extent.createTest("ClICK ON NUTRITION BUTTON","This test case will click on nutrition button");
		test.log(Status.INFO, "Test Case Started");
		driver.findElement(By.id("nutrition_box")).click();
		test.log(Status.PASS, "nutrition box is clicked");
		Thread.sleep(5000);
	}
	@Test(priority=4 , enabled=true)
	void waterclick() throws InterruptedException
	{
		test = extent.createTest("CLICK ON WATER","This test case will click on water");
		test.log(Status.INFO, "Test Case Started");
		SoftAssert assertion = new  SoftAssert();
		MobileElement element1=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout//android.widget.ImageView[@index=0]"));
		assertion.assertTrue(element1.isDisplayed());
		element1.click();
		MobileElement element2=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout//android.view.View[@index=0]"));
		element2.click();	
		test.log(Status.PASS, "water button is clicked");
		Thread.sleep(2000);
	}
	@Test(priority=5, enabled=true)
	void addwaterdata() throws InterruptedException
	{
		test = extent.createTest("ADD WATER DATA","This test case will add dairy data");
		test.log(Status.INFO, "Test Case Started");
		add_subtractdata();
		servingguide();
		savedata();
		Thread.sleep(3000);	
	}

	@Test(priority=6, enabled=true)

	void clickdairy() throws InterruptedException
	{
		test = extent.createTest("CLICK ON DAIRY","This test case will click on dairy");
		test.log(Status.INFO, "Test Case Started");
		MobileElement element1=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout[@index=1]//android.widget.ImageView[@index=0]"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(element1.isDisplayed());
		element1.click();

		MobileElement element2=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout[@index=1]//android.view.View[@index=0]"));
		element2.click();

		test.log(Status.PASS, "dairy button is clicked");
		//Thread.sleep(3000);

	}
	@Test(priority=7, enabled=true)

	void adddairy() throws InterruptedException
	{
		test = extent.createTest("ADD DAIRY DATA","This test case will add dairy data");
		test.log(Status.INFO, "Test Case Started");
		add_subtractdata();
		servingguide();
		savedata();
		Thread.sleep(3000);
	}
	@Test(priority=8, enabled=true)

	void clickgrains() throws InterruptedException
	{
		test = extent.createTest("CLICK ON GRAINS","This test case will click on grains");
		test.log(Status.INFO, "Test Case Started");
		MobileElement element1=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout[@index=2]//android.widget.ImageView[@index=0]"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(element1.isDisplayed());
		element1.click();

		MobileElement element2=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout[@index=2]//android.view.View[@index=0]"));
		element2.click();

		test.log(Status.PASS, "grains button clicked successfully");
		//Thread.sleep(3000);

	}
	@Test(priority=9, enabled=true)

	void addgrains() throws InterruptedException
	{test = extent.createTest("ADD GRAINS DATA","This test case will add grains data");
		add_subtractdata();
		servingguide();
		savedata();
		Thread.sleep(3000);
	}


	@Test(priority=10, enabled=true)

	void clickfruits_veggies() throws InterruptedException
	{
		test = extent.createTest("CLICK ON FRUIT/VEGGIES","This test case will click on fruit/veggies");
		test.log(Status.INFO, "Started");
		MobileElement element1=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout[@index=2]//android.widget.RelativeLayout[@index=0]//android.widget.ImageView[@index=0]"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(element1.isDisplayed());
		element1.click();

		MobileElement element2=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout[@index=2]//android.widget.RelativeLayout[@index=0]//android.view.View[@index=0]"));
		element2.click();

		test.log(Status.PASS, "Fruits/veggies button clicked successfully");
		//Thread.sleep(3000);

	}
	@Test(priority=11, enabled=true)

	void addfruits_veggies() throws InterruptedException
	{
		test = extent.createTest("ADD FRUIT/VEGGIOES DATA","This test case will add fruit/veggies data");
		add_subtractdata();
		servingguide();
		savedata();
		Thread.sleep(3000);
	}
	@Test(priority=12, enabled=true)

	void clickmeat_legumes() throws InterruptedException
	{
		test = extent.createTest("CLICK ON MEAT/LEGUMES","This test case will click on Meat/legumes");
		test.log(Status.INFO, "Started");
		MobileElement element1=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout[@index=2]//android.widget.RelativeLayout[@index=1]//android.widget.ImageView[@index=0]"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(element1.isDisplayed());
		element1.click();

		MobileElement element2=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout[@index=2]//android.widget.RelativeLayout[@index=1]//android.view.View[@index=0]"));
		element2.click();

		test.log(Status.PASS, "Fruits/veggies button clicked successfully");
		//Thread.sleep(3000);

	}
	@Test(priority=13, enabled=true)

	void addmeat_legumes() throws InterruptedException
	{
		test = extent.createTest("ADD MEAT/LEGUMES DATA","This test case will add fruit/veggies data");
		add_subtractdata();
		servingguide();
		savedata();
		Thread.sleep(3000);
	}
	@Test(priority=14, enabled=true)
	void deletenutrition_click()
	{   	test = extent.createTest("CLICK ON DELETE BUTTON","This test case will click on delete button");
	test.log(Status.INFO, "Started");
	driver.findElement(By.id("iv_delete_records")).click();
	test.log(Status.PASS, "Delete button clicked successfully");
	}
	@Test(priority=15, enabled=true)

	void resetall() throws InterruptedException
	{

		test = extent.createTest("RESET NUTRITION DATA","This test case will reset nutrition data");
		test.log(Status.INFO, "Started");
		driver.findElement(By.id("btn_delete_all")).click();
		test.log(Status.PASS, "Reset All button clicked successfully");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		test.log(Status.PASS, "Nutrition score deleted successfully");	
		Thread.sleep(3000);
	}
	@Test(priority=16, enabled=true)
	void checkalreadydeletednutritiondata() throws InterruptedException
	{
		test = extent.createTest("RESET NUTRITION DATA AGAIN","This test case will reset nutrition data again to verify toast message");
		test.log(Status.INFO, "Started");
		deletenutrition_click();
		resetall();
		test.log(Status.INFO, "Screen shot taken to verify toast if data is not present");
		screenshot();
		test.log(Status.PASS, "Screenshot taken successfully");
		String Text= OCR(scrPath);
		System.out.println(Text);
		SoftAssert softassert = new SoftAssert();
		softassert.assertEquals(Text.contains("\"Nutrition history is not available, Please add your nutrition data first.\";"), Text.equals("\"Nutrition history is not available, Please add your nutrition data first.\";"));
		try {
			test.log(Status.PASS, "Toast Verified . there is no data please add data and then tries to delete");
			WebDriverWait wait=new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_menu_back")));
			driver.findElement(By.id("iv_menu_back")).click();
			Thread.sleep(5000);
		}
		catch(AssertionError e)
		{
			test.log(Status.FAIL, e);
		}
	}
	@Test(priority=17 , enabled=true)
	void waterclick1() throws InterruptedException
	{
		test = extent.createTest("CLICK ON WATER","This test case will click on water");
		test.log(Status.INFO, "Test Case Started");
		SoftAssert assertion = new  SoftAssert();
		MobileElement element1=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout//android.widget.ImageView[@index=0]"));
		assertion.assertTrue(element1.isDisplayed());
		element1.click();
		MobileElement element2=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout//android.view.View[@index=0]"));
		element2.click();	
		test.log(Status.PASS, "water button is clicked");
		Thread.sleep(2000);
	}
	@Test(priority=18, enabled=true)
	void addwaterdata1() throws InterruptedException
	{
		test = extent.createTest("ADD WATER DATA","This test case will add dairy data");
		test.log(Status.INFO, "Test Case Started");
		add_subtractdata();
		servingguide();
		savedata();
		Thread.sleep(3000);	
	}

	@Test(priority=19, enabled=true)

	void clickdairy1() throws InterruptedException
	{
		test = extent.createTest("CLICK ON DAIRY","This test case will click on dairy");
		test.log(Status.INFO, "Test Case Started");
		MobileElement element1=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout[@index=1]//android.widget.ImageView[@index=0]"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(element1.isDisplayed());
		element1.click();

		MobileElement element2=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout[@index=1]//android.view.View[@index=0]"));
		element2.click();

		test.log(Status.PASS, "dairy button is clicked");
		//Thread.sleep(3000);

	}
	@Test(priority=20, enabled=true)

	void adddairy1() throws InterruptedException
	{
		test = extent.createTest("ADD DAIRY DATA","This test case will add dairy data");
		test.log(Status.INFO, "Test Case Started");
		add_subtractdata();
		servingguide();
		savedata();
		Thread.sleep(3000);
	}
	@Test(priority=21, enabled=true)

	void clickgrains1() throws InterruptedException
	{
		test = extent.createTest("CLICK ON GRAINS","This test case will click on grains");
		test.log(Status.INFO, "Test Case Started");
		MobileElement element1=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout[@index=2]//android.widget.ImageView[@index=0]"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(element1.isDisplayed());
		element1.click();

		MobileElement element2=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout//android.widget.RelativeLayout[@index=2]//android.view.View[@index=0]"));
		element2.click();

		test.log(Status.PASS, "grains button clicked successfully");
		//Thread.sleep(3000);

	}
	@Test(priority=22, enabled=true)

	void addgrains1() throws InterruptedException
	{test = extent.createTest("ADD GRAINS DATA","This test case will add grains data");
		add_subtractdata();
		servingguide();
		savedata();
		Thread.sleep(3000);
	}


	@Test(priority=23, enabled=true)

	void clickfruits_veggies1() throws InterruptedException
	{
		test = extent.createTest("CLICK ON FRUIT/VEGGIES","This test case will click on fruit/veggies");
		test.log(Status.INFO, "Started");
		MobileElement element1=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout[@index=2]//android.widget.RelativeLayout[@index=0]//android.widget.ImageView[@index=0]"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(element1.isDisplayed());
		element1.click();

		MobileElement element2=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout[@index=2]//android.widget.RelativeLayout[@index=0]//android.view.View[@index=0]"));
		element2.click();

		test.log(Status.PASS, "Fruits/veggies button clicked successfully");
		//Thread.sleep(3000);

	}
	@Test(priority=24, enabled=true)

	void addfruits_veggies1() throws InterruptedException
	{
		test = extent.createTest("ADD FRUIT/VEGGIOES DATA","This test case will add fruit/veggies data");
		add_subtractdata();
		servingguide();
		savedata();
		Thread.sleep(3000);
	}
	@Test(priority=25, enabled=true)

	void clickmeat_legumes1() throws InterruptedException
	{
		test = extent.createTest("CLICK ON MEAT/LEGUMES","This test case will click on Meat/legumes");
		test.log(Status.INFO, "Started");
		MobileElement element1=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout[@index=2]//android.widget.RelativeLayout[@index=1]//android.widget.ImageView[@index=0]"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(element1.isDisplayed());
		element1.click();

		MobileElement element2=	driver.findElement(By.xpath("//android.widget.RelativeLayout//android.widget.LinearLayout[@index=2]//android.widget.RelativeLayout[@index=1]//android.view.View[@index=0]"));
		element2.click();

		test.log(Status.PASS, "Fruits/veggies button clicked successfully");
		//Thread.sleep(3000);

	}
	@Test(priority=26, enabled=true)

	void addmeat_legumes1() throws InterruptedException
	{
		test = extent.createTest("ADD MEAT/LEGUMES DATA","This test case will add fruit/veggies data");
		add_subtractdata();
		servingguide();
		savedata();
		Thread.sleep(3000);
	}
	@Test(priority=27, enabled=true)
	void deletewater() throws InterruptedException
	{
		deletenutrition_click();
		test = extent.createTest("DELETE WATER DATA","This test case will delete water data");
		test.log(Status.INFO, "Started");
		driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@index=0]//android.widget.ImageView[@index=1]")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		test.log(Status.PASS, "water data deleted successfully");	

	}
	@Test(priority=28, enabled=true)

	void deletedairy() throws InterruptedException
	{
		deletenutrition_click();
		test = extent.createTest("DELETE DAIRY DATA","This test case will delete dairy data");
		test.log(Status.INFO, "Started");
		driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@index=1]//android.widget.ImageView[@index=1]")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		test.log(Status.PASS, "dairy data deleted successfully");	
	}
	@Test(priority=29, enabled=true)

	void deletegrains() throws InterruptedException
	{
		deletenutrition_click();
		test = extent.createTest("DELETE GRAINS DATA","This test case will delete grains data");
		test.log(Status.INFO, "Started");
		driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@index=2]//android.widget.ImageView[@index=1]")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		test.log(Status.PASS, "grains data deleted successfully");	
	}
	@Test(priority=30, enabled=true)

	void deletefruitveggies() throws InterruptedException
	{
		deletenutrition_click();
		test = extent.createTest("DELETE FRUIT/VEGGIES DATA","This test case will delete fruit/veggies data");
		test.log(Status.INFO, "Started");
		driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@index=3]//android.widget.ImageView[@index=1]")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		test.log(Status.PASS, "fruit/veggies data deleted successfully");	
	}
	@Test(priority=31, enabled=true)

	void deletemeatlegumes() throws InterruptedException
	{
		deletenutrition_click();
		test = extent.createTest("DELETE MEAT/LEGUMES DATA","This test case will delete MEAT/LEGUMES data");
		test.log(Status.INFO, "Started");
		driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@index=4]//android.widget.ImageView[@index=1]")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		test.log(Status.PASS, "meat/legumes data deleted successfully");
		driver.findElement(By.id("iv_menu_back")).click();
		Thread.sleep(5000);
	}
	@Test(priority=32 , enabled=true)
	void homescreen() throws InterruptedException
	{
		test = extent.createTest("BACK TO HOME SCREEN","This test case will navigate to home screen");
		driver.findElement(By.id("iv_home_logo")).click();
		Thread.sleep(5000);
	}
	
}
