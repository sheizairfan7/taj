package tests;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
/**
 * Created by Shanza Irfan Qureshi
 */
public class ExtendTestLogin {

	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest test;
	public  AppiumDriver<MobileElement> driver;

	@BeforeTest
	public void startReport() throws MalformedURLException {


		DesiredCapabilities capabilities= new DesiredCapabilities();

		capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.taj.patient.debug");
		capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.taj.patient.view.activity.SplashActivity");
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
		//capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Andriod");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "SAMSUNG J7 MAX");
		//capabilities.setCapability(MobileCapabilityType.UDID, udid);
		capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS,true);
		//capabilities.setCapability(AndroidMobileCapabilityType.SYSTEM_PORT, systemPort);

		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AndroidDriver<MobileElement>(url,capabilities);

		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);



		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/LoginExtentReport.html");
		// Create an object of Extent Reports
		extent = new ExtentReports();  
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Host Name", "SoftwareTestingMaterial");
		extent.setSystemInfo("Environment", "Production");
		extent.setSystemInfo("User Name", "Rajkumar SM");
		htmlReporter.config().setDocumentTitle("Title of the Report Comes here "); 
		// Name of the report
		htmlReporter.config().setReportName("Name of the Report Comes here "); 
		// Dark Theme
		htmlReporter.config().setTheme(Theme.STANDARD);			
	}

	//This method is to capture the screenshot and return the path of the screenshot.
	public  String getScreenShot(AppiumDriver<MobileElement> driver2, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver2;
		File source = ts.getScreenshotAs(OutputType.FILE);
		// after execution, you could see a folder "FailedTestsScreenshots" under src folder
		String destination = System.getProperty("user.dir") + "/Screenshots/" + screenshotName + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}
	@BeforeMethod
	public void beforemethod() {

	}

	@Test(priority=0)
	public void appLanguageScreen() {

		test =extent.createTest("SELECT APPLICATION LANGUAGE","This test case will load language screen and select language");
		test.log(Status.INFO,"Test case Started");	

		WebDriverWait wait= new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_eng")));	

		driver.findElement(By.id("iv_eng")).click();
		test.log(Status.PASS, "English Language is selected");

		driver.findElement(By.id("linearLayout3")).click();
		test.log(Status.PASS, "Continue Button is clicked");	

	}

	@Test(priority=1)
	public void enterMobileNumber() {

		test =extent.createTest("ENTER MOBILE NUMBER","This test case will Enter Mobile Number");
		test.log(Status.INFO,"Test case Started");	

		WebDriverWait wait= new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("et_mobile_num_")));	

		driver.findElement(By.id("et_mobile_num_")).sendKeys("89988998");
		test.log(Status.PASS, "Mobile Number is entered");

		driver.findElement(By.id("linearLayout3")).click();
		test.log(Status.PASS, "Continue Button is clicked");	

	}

	@Test(priority=2)
	public void enterVerificationCode() {

		test =extent.createTest("ENTER VERIFICATION CODE","This test case will Enter 010101 Verification code");
		test.log(Status.INFO,"Test case Started");	

		driver.findElement(By.id("et_verification_code")).sendKeys("0");
		driver.findElement(By.id("et_verification_code2")).sendKeys("1");
		driver.findElement(By.id("et_verification_code3")).sendKeys("0");
		driver.findElement(By.id("et_verification_code4")).sendKeys("1");
		driver.findElement(By.id("et_verification_code5")).sendKeys("0");
		driver.findElement(By.id("et_verification_code6")).sendKeys("1");

		test.log(Status.PASS, "OPTP Entered");	
	}

	@Test(priority=3)
	public void enterPin() {

		String text =driver.findElement(By.id("tv_title_security")).getText();

		SoftAssert assertion= new SoftAssert();
		assertion.assertEquals(text, text.equalsIgnoreCase("Enter PIN"));
		test =extent.createTest("ENTER PIN","This test case will Enter pin 1111");
		test.log(Status.INFO,"Test case Started");	

		driver.findElement(By.id("et_security_pin_digit_1")).sendKeys("1");
		driver.findElement(By.id("et_security_pin_digit_2")).sendKeys("3");
		driver.findElement(By.id("et_security_pin_digit_3")).sendKeys("4");
		driver.findElement(By.id("et_security_pin_digit_4")).sendKeys("1");
	}

	@Test(priority=4)
	public void onHome() {
		String element=driver.findElement(By.id("view_empty")).getText();
		String text="L I  F  E  S  T  Y  L  E      S  g  O  R  E";
		SoftAssert assertion = new SoftAssert();
		assertion.assertTrue(text.equalsIgnoreCase(element));		
	}


	@AfterMethod
	public void getResult(ITestResult result) throws Exception{
		if(result.getStatus() == ITestResult.FAILURE){
			//MarkupHelper is used to display the output in different colors
			test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
			test.log(Status.FAIL, MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
			//To capture screenshot path and store the path of the screenshot in the string "screenshotPath"
			//We do pass the path captured by this method in to the extent reports using "logger.addScreenCapture" method. 
			//String Scrnshot=TakeScreenshot.captuerScreenshot(driver,"TestCaseFailed");
			String screenshotPath = getScreenShot(driver, result.getName());
			//To add it in the extent report 
			test.fail("Test Case Failed Snapshot is below " + test.addScreenCaptureFromPath(screenshotPath));
		}
		else if(result.getStatus() == ITestResult.SKIP){
			test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE)); 
		} 
		else if(result.getStatus() == ITestResult.SUCCESS)
		{
			test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		}
	}

	@AfterTest
	public void endReport() {
		extent.flush();
		driver.quit();
	}
}

