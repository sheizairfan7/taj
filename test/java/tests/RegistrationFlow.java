package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

/**
 * Created by Javaria Naeem
 */

public class RegistrationFlow extends BaseClass {



	ExtentTest test;
	Point p;
	WebDriverWait wait;


	@Test(priority=0)
	void selectlanguagescreen() 
	{
		test = extent.createTest("Test Case to select application language");
		test.log(Status.INFO, "Started");
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_eng")));	
		//SELECT APPLICATION LANGUAGE
		driver.findElement(By.id("iv_eng")).click();
		driver.findElement(By.id("linearLayout3")).click();
		test.log(Status.PASS, "Language is selected");

	}
	@Test(priority=1)
	void entermobileNumber() 
	{
		test = extent.createTest("Test Case to Enter Mobile Number");
		test.log(Status.INFO, "Started");
		WebDriverWait wait = new WebDriverWait(driver,10);
		//Enter Mobile Number
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("et_mobile_num_")));
		MobileElement mobilenumber=driver.findElement(By.id("et_mobile_num_"));
		mobilenumber.sendKeys("89988998");
		driver.findElement(By.id("linearLayout3")).click();
		test.log(Status.PASS, "Number is entered");
	}


	@Test(priority=2)
	void enterotp() throws InterruptedException
	{
		test = extent.createTest("Test Case Enter to OTP");
		test.log(Status.INFO, "Started");
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_validation_info")));

		driver.findElement(By.id("et_verification_code")).sendKeys("0");
		driver.findElement(By.id("et_verification_code2")).sendKeys("1");
		driver.findElement(By.id("et_verification_code3")).sendKeys("0");
		driver.findElement(By.id("et_verification_code4")).sendKeys("1");
		driver.findElement(By.id("et_verification_code5")).sendKeys("0");
		driver.findElement(By.id("et_verification_code6")).sendKeys("1");
		test.log(Status.PASS, "OTP is entered");

	}


	@Test(priority=3)
	void enterpin() 
	{
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_title_security")));

		String  Atitle=driver.findElement(By.id("tv_title_security")).getText();
		String Etitle="Enter PIN";
		SoftAssert softassert = new SoftAssert();
		softassert.assertEquals(Atitle, Etitle);
		try
		{
			test = extent.createTest("Test Case to Enter PIN");
			test.log(Status.INFO, "Started");
			driver.findElement(By.id("et_security_pin_digit_1")).sendKeys("1");
			driver.findElement(By.id("et_security_pin_digit_2")).sendKeys("1");
			driver.findElement(By.id("et_security_pin_digit_3")).sendKeys("1");
			driver.findElement(By.id("et_security_pin_digit_4")).sendKeys("1");
			test.log(Status.PASS, "Pin entered successfully");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home_top")));
			test.log(Status.INFO, "Login Successfull Home screen appears");
	
		}
		catch(AssertionError e)
		{
			test.log(Status.FAIL, e);
		}
	

		
	}


	// if title is "Create New PIN" user is registered but pin is not created"
	@Test(priority=4)
	void createpin() 
	{
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_title_security")));

		String  title=driver.findElement(By.id("tv_title_security")).getText();
		SoftAssert softassert = new SoftAssert();
		softassert.assertEquals(title, title.equals("Create PIN")||title.equals("CREATE PIN"));
		try

		{
			test = extent.createTest("Test Case to create pin for already registered patient");
			test.log(Status.INFO, "Started");
			driver.findElement(By.id("et_security_new_pin_digit_1")).sendKeys("1");
			driver.findElement(By.id("et_security_new_pin_digit_2")).sendKeys("1");
			driver.findElement(By.id("et_security_new_pin_digit_3")).sendKeys("1");
			driver.findElement(By.id("et_security_new_pin_digit_4")).sendKeys("1");
			test.log(Status.PASS, " New Pin entered successfully");
			driver.findElement(By.id("et_security_confirm_pin_digit_1")).sendKeys("1");
			driver.findElement(By.id("et_security_confirm_pin_digit_2")).sendKeys("1");
			driver.findElement(By.id("et_security_confirm_pin_digit_3")).sendKeys("1");
			driver.findElement(By.id("et_security_confirm_pin_digit_4")).sendKeys("1");
			test.log(Status.PASS, "Confirm Pin entered successfully");
			driver.findElement(By.id("view_security_pin_next")).click();
			test.log(Status.PASS, "Pin entered successfully");
			
		}
		catch(AssertionError e)
		{
			test.log(Status.FAIL, e);
		}
		
	}
	@Test(priority=5)
	void setsecurityans()
	{
	
			
	// Select Security Question
	        WebDriverWait wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_question_title")));
			test = extent.createTest("Test Case to set answer for selected security question");
			test.log(Status.INFO, "Started");
			driver.findElement(By.id("et_answer_comments")).sendKeys("abc");
			test.log(Status.PASS, "Answer entered successfully");
			driver.findElement(By.id("tv_continue")).click(); 
			test.log(Status.PASS, "Answer created successfully");


		}	

	@Test(priority=6)
	void enternationalid() 
	{
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_nic")));
		String title=driver.findElement(By.id("tv_nic")).getText();	
		SoftAssert softassert = new SoftAssert();
		softassert.assertEquals(title, title.equals("Insurance Verification")||title.equals("INSURANCE VERIFICATION"));
		try
		{

			test = extent.createTest("Test Case to enter national ID");
			test.log(Status.INFO, "Started");
			driver.findElement(By.id("et_id")).sendKeys("1084409356");
			test.log(Status.INFO, "National ID Entered Successfully");
			// Check Terms and Conditions Check Box

			MobileElement e1 =driver.findElement(By.xpath("//android.widget.CheckBox[@index=0]"));
			Point p=e1.getLocation();	

			TouchAction touchAction = new TouchAction(driver);
			touchAction.tap(PointOption.point(p.x,p.y)).perform();
			driver.findElement(By.id("view_next_btn")).click();
			test.log(Status.INFO, "terms and condition selected");
			// Details
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_congrats")));
			driver.findElement(By.id("btn_next")).click();
			test.log(Status.INFO, "Details verified successfully");
			
		}
		catch(AssertionError e)
		{
			test.log(Status.FAIL, e);
		}

	}


	@Test(priority=7)
	void selectdoctlang() 
	{
		// Select Language
		test = extent.createTest("Test Case to select doctor's language");
		test.log(Status.INFO, "Started");
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_title")));
		driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView//android.widget.TextView[@index=1]"));
		driver.findElement(By.id("tv_continue")).click();
		test.log(Status.INFO, "Doctors language selected successfully");
	}

	@Test(priority=8)
	void selectdoctgender() 
	{
		test = extent.createTest("Test Case to select doctor's gender");
		test.log(Status.INFO, "Started");
		//Select Gender
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_title")));
		driver.findElement(By.id("layout_double")).click();
		driver.findElement(By.id("tv_continue")).click();
		test.log(Status.INFO, "Doctors language selected successfully");
	}
	@Test(priority=9)
	void createnewpin() 
	{
		test = extent.createTest("Test Case to create pin for new user");
		test.log(Status.INFO, "Started");

		// Create Pin
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_title_security")));

		driver.findElement(By.id("et_security_new_pin_digit_1")).sendKeys("1");
		driver.findElement(By.id("et_security_new_pin_digit_2")).sendKeys("1");
		driver.findElement(By.id("et_security_new_pin_digit_3")).sendKeys("1");
		driver.findElement(By.id("et_security_new_pin_digit_4")).sendKeys("1");
		test.log(Status.INFO, "new pin enter successfully");
		driver.findElement(By.id("et_security_confirm_pin_digit_1")).sendKeys("1");
		driver.findElement(By.id("et_security_confirm_pin_digit_2")).sendKeys("1");
		driver.findElement(By.id("et_security_confirm_pin_digit_3")).sendKeys("1");
		driver.findElement(By.id("et_security_confirm_pin_digit_4")).sendKeys("1");
		test.log(Status.INFO, "Confirm pin enter successfully");
		driver.findElement(By.id("view_security_pin_next")).click();
		test.log(Status.INFO, "new pin created successfully");
	}  	
	@Test(priority=10)
	void setsecurityanwser() 
	{
		test = extent.createTest("Test Case to set answer for security question");
		test.log(Status.INFO, "Started");

		// Select Security Question
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_question_title")));

		driver.findElement(By.id("et_answer_comments")).sendKeys("abc");
		driver.findElement(By.id("tv_continue")).click(); 
		test.log(Status.INFO, "security Answer saved successfully");
		test.log(Status.INFO, "User registered successfully");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home_top")));

	}

}



