package tests;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utilities.ActionMethods;

/**
 * Created by Javaria Naeem
 */


public class MenuConsultations extends BaseClass {

	ExtentTest test;
	Point p;
	WebDriverWait wait;

	@Test(priority=1)
	public void menu() throws InterruptedException 
	{
		test = extent.createTest("ClICK ON MENU","This test case will click on menu button");
		test.log(Status.INFO, "Test case Started");
		//Click on menu button
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_logo_menu")));
		MobileElement e1 =driver.findElement(By.id("iv_logo_menu"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Menu button is clicked");
		Thread.sleep(5000);
	}
	@Test(priority=2)
	public void consultationsClick() throws InterruptedException 
	{
		test = extent.createTest("VIEW CONSULTATIONS","This test case will view consultations of patient");
		test.log(Status.INFO, "Test case Started");
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Consultations']")));
		MobileElement e1 =driver.findElement(By.xpath("//android.widget.TextView[@text='Consultations']"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Consultation button is clicked");
		Thread.sleep(5000);
	}
	@Test(priority=3)
	public void noconsultation() throws InterruptedException 
	{
		SoftAssert assertion=new SoftAssert();
		MobileElement ee2=driver.findElement(By.id("view_no_item"));
		assertion.assertTrue(ee2.isDisplayed());
		test.log(Status.PASS, "No consultation history");
	}
	@Test(priority=4)
	public void approvedpres() throws InterruptedException 
	{
		SoftAssert assertion=new SoftAssert();
		//prescription Approved/pending
		
		MobileElement e2=driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView[@index=1]//android.widget.RelativeLayout[@index=0]//android.widget.RelativeLayout[@index=1]//android.widget.LinearLayout[@index=1]//android.widget.ImageView[@index=3]"));
		assertion.assertTrue(e2.isDisplayed());
		e2.click();
		test.log(Status.PASS, "Approved/pending/denied prescription opened successfully");
		Thread.sleep(5000);
	}
	@Test(priority=5)
	public void approvedpreswithfollowup() throws InterruptedException 
	{
		SoftAssert assertion=new SoftAssert();
		//prescription Approved/pending
		MobileElement e2=driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView[@index=1]//android.widget.RelativeLayout[@index=0]//android.widget.RelativeLayout[@index=1]//android.widget.LinearLayout[@index=1]//android.widget.ImageView[@index=5]"));
		assertion.assertTrue(e2.isDisplayed());
		e2.click();
		test.log(Status.PASS, "Approved/pending/denied prescription opened successfully");
		Thread.sleep(5000);
	}
	@Test(priority=6)
	public void nomedpres() throws InterruptedException 
	{
		//Nomedicine
		MobileElement e3=driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView[@index=1]//android.widget.RelativeLayout[@index=0]//android.widget.RelativeLayout[@index=1]//android.widget.LinearLayout[@index=1]//android.widget.ImageView[@index=1]"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(e3.isDisplayed());
		e3.click();
		test.log(Status.PASS, "prescription with no medicine opened successfully");
		
	}
	@Test(priority=7)
	public void errorpres() throws InterruptedException 
	{
		//Error Prescription
		MobileElement e4=driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView[@index=1]//android.widget.RelativeLayout[@index=0]//android.widget.RelativeLayout[@index=1]//android.widget.LinearLayout[@index=1]//android.widget.TextView[@index=1]"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(e4.isDisplayed());
		ActionMethods.tapOnElement(e4);
		test.log(Status.PASS, "Errored prescription opened successfully");
		Thread.sleep(5000);
	}
	@Test(priority=8)
	public void ratedoc() throws InterruptedException 
	{
		driver.findElement(By.id("iv_menu_back")).click();
		
		MobileElement e1=driver.findElement(By.id("ratingBar2"));
		SoftAssert assertion=new SoftAssert();
		assertion.assertTrue(e1.isDisplayed());
		ActionMethods.tapOnElement(e1);
		MobileElement e2=driver.findElement(By.id("tv_rateDoctor"));
		e2.click();	
		test.log(Status.PASS, "rate doctor successfully");
		Thread.sleep(5000);
	}
	@Test(priority=9)
	public void backtomenu() throws InterruptedException 
	{
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_menu_back")));
		driver.findElement(By.id("iv_menu_back")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_menu_back")));
		driver.findElement(By.id("iv_menu_back")).click();
		Thread.sleep(5000);
	}
}
