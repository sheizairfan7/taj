package tests;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utilities.ActionMethods;

public class HealthScoreMind  extends ToastVeification {

	ExtentTest test;
	Point p;
	WebDriverWait wait;
	@Test(priority=1,enabled=true)
	void lifestylescore() throws InterruptedException
	{
		test = extent.createTest("ClICK ON LIFESTYLESCORE","This test case will click on lifestyle score");
		test.log(Status.INFO, "Test Case Started");
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("health_score")));
		driver.findElement(By.id("health_score")).click();
		test.log(Status.PASS, "Heathscore button is clicked");	
		Thread.sleep(5000);	

	}

	//Test Scenario 3: Verify that user can add HealthScore_Mind data
	@Test(priority=2,enabled=true)
	void lifestylescoremind() throws InterruptedException
	{
		test = extent.createTest("ClICK ON MIND","This test case will click on mind button");
		test.log(Status.INFO, "Test Case Started");
		driver.findElement(By.id("mind_box")).click();
		test.log(Status.PASS, " Mind button is clicked");

	}

	@Test(priority=3 , enabled=true)
	void addsleepdata() throws InterruptedException
	{

		test = extent.createTest("ADD SLEEP DATA","This test case will add sleep data");
		test.log(Status.INFO, "Test Case Started");
		MobileElement e1= driver.findElement(By.id("pluse_add"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Add "+" button is clicked ");
		MobileElement seek_bar=driver.findElement(By.id("clock_front"));
		ActionMethods.tapOnElement(seek_bar); //needs to check verticallydown method may b used
		test.log(Status.PASS, "Sleep duration is added");
		driver.findElement(By.id("bt_done")).click();
		test.log(Status.PASS, "Save button is clicked  and  sleep score updated");
		String sleeppc=driver.findElement(By.id("fittnes_score")).getText();
		System.out.println(sleeppc);
		Thread.sleep(3000);
	}

	@Test(priority=4,enabled=true)
	void addcalmnessdata() throws InterruptedException
	{
		test = extent.createTest("ADD CALMNESS DATA","This test case will add sleep data");
		test.log(Status.INFO, "Test Case Started");
		MobileElement e1= driver.findElement(By.id("stress_add"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Add "+" button is clicked");
		MobileElement e2= driver.findElement(By.id("custom_check_box"));
		ActionMethods.tapOnElement(e2);
		test.log(Status.PASS, "Calmness value is checked ");
		driver.findElement(By.id("btn_cal_score")).click();
		test.log(Status.PASS, "Save button is clicked and calmness score updated");
		test.log(Status.PASS, "Mind Score is updated ");
	}

	//Test Scenario 4: Verify that user can delete (Reset All ) HealthScore_Mind  data

	@Test(priority=5 , enabled=true)
	void deletemindscore() throws InterruptedException
	{
		test = extent.createTest("DELETE MIND SCORE","This test case will click on delete button");
		test.log(Status.INFO, "Test Case Started");
		driver.findElement(By.id("iv_delete_records")).click();
		test.log(Status.PASS, "delete button is clicked");	
	}
	@Test(priority=6 , enabled=true)
	void resetall() throws InterruptedException
	{
		test = extent.createTest("RESET MIND SCORE","This test case will reset mind score");
		test.log(Status.INFO, "Test Case Started");
		driver.findElement(By.id("btn_delete_all")).click();
		test.log(Status.PASS, "Reset All button is clicked ");	
		Thread.sleep(3000);
		driver.findElement(By.id("btn_done")).click();
		test.log(Status.PASS, "Mind is score deleted ");	
		Thread.sleep(3000);
	}

	//Test Scenario 5 : Verify that user can not delete (Reset All ) HealthScore_Mind  data once he already reset it
	@Test (priority=7, enabled=true)
	void checkalreadydeleted() throws InterruptedException
	{
		test = extent.createTest("RESET MIND SCORE AGAIN","This test case will try to reset mind score again");
		test.log(Status.INFO, "Test Case Started");
		deletemindscore();
		resetall();
		test.log(Status.INFO, "Screen shot taken to verify toast if data is not present");
		screenshot();
		test.log(Status.PASS, "Screenshot is taken ");
		String Text= OCR(scrPath);
		System.out.println(Text);
		SoftAssert softassert = new SoftAssert();
		softassert.assertEquals(Text.contains("\"Mind history is not available, Please add your mind data first.\";"), Text.equals("\"Mind history is not available, Please add your mind data first.\";"));
		try {
			test.log(Status.PASS, "Toast Verified . there is no data please add data and then tries to delete");
			WebDriverWait wait=new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_menu_back")));
			driver.findElement(By.id("iv_menu_back")).click();
		}
		catch(AssertionError e)
		{
			test.log(Status.FAIL, e);
		}


	}
	//Test Scenario 5 : Verify that user can delete sleep and calmness individually

	@Test(priority=8 , enabled=true)
	void adddata() throws InterruptedException
	{
		test = extent.createTest("ADD MIND SCORE AGAIN","This test case will add mind data again");
		test.log(Status.INFO, "Started");
		addsleepdata();
		addcalmnessdata();
		deletemindscore();
	}
	@Test(priority=9 , enabled=true)
	void deletSleepcore() throws InterruptedException
	{
		test = extent.createTest("DELETE SLEEP SCORE","This test case will delete sleep score individually");
		test.log(Status.INFO, "Started");
		driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView[@index=3]//android.view.ViewGroup[@index=0]//android.widget.ImageView[@index=1]")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		test.log(Status.PASS, "Sleep score is deleted");	
		Thread.sleep(3000);
	}
	@Test(priority=10 , enabled=true)
	void deletCalmnessscore() throws InterruptedException
	{
		deletemindscore();
		test = extent.createTest("DELETE CALMNESS SCORE","This test case will delete calmness score individually");
		test.log(Status.INFO, "Started");
		driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView[@index=3]//android.view.ViewGroup[@index=1]//android.widget.ImageView[@index=1]")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		//driver.findElement(By.id("btn_done")).click();
		test.log(Status.PASS, "Calmness score is deleted");	
		Thread.sleep(3000);
		driver.findElement(By.id("iv_menu_back")).click();
		test.log(Status.PASS, "back button is clicked");	
		Thread.sleep(3000);
	}
	@Test(priority=11 , enabled=true)
	void homescreen() throws InterruptedException
	{
		driver.findElement(By.id("iv_home_logo")).click();
		test.log(Status.PASS, "navigate to home screen");	
		Thread.sleep(5000);
	}
}

