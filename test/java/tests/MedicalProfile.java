package tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

/**
 * Created by Shanza Irfan Qureshi
 */
public class MedicalProfile extends BaseClass {
	ExtentTest test;

	@Test(priority=1,enabled=false)
	public void clickOnMenuIcon(){
		test =extent.createTest("CLICK MENU ICON","This Test case will click on Menu Icon to select medical profile From Home Screen.");
		test.log(Status.INFO,"Test case Started");	

		driver.findElement(By.id("iv_logo_menu")).click();	
		test.log(Status.INFO, "clicked Menu Icon on Home Screen");
	}

	@Test(priority=2)
	public void tapOnMedicalprofile(){

		test =extent.createTest("CLICK MEDICAL PROFILE","This Test Case will click on medical profile.");
		test.log(Status.INFO,"Test case Started");

		MobileElement medicalProfile =driver.findElement(By.xpath("//android.widget.TextView[@text='Medical Profile']"));
		ActionMethods.tapOnElement(medicalProfile);
		test.log(Status.PASS, "Medical profile clicked");
	}

	@Test(priority=3,groups={"personalProfile","all"})
	public void personalProfileSetHeight() throws InterruptedException{	

		test =extent.createTest("PERSONAL PROFILE SET HEIGHT","This Test Case will set the height of patient.");
		test.log(Status.INFO,"Test case Started");

		driver.findElement(By.id("tv_height")).click();
		test.log(Status.PASS, "Clicked on Height");

		MobileElement addheight = driver.findElement(By.id("wheel_height"));
		ActionMethods.verticallyUp(addheight, 200);
		test.log(Status.PASS, "Select value from list");

		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on save button");
	}

	@Test(priority=4,groups={"personalProfile","all"})
	public void verifyCancelButtonSetHeight(){	

		test =extent.createTest("SET HEIGHT CANCEL BUTTON","This Test Case will verify cancel Button is Functional.");
		test.log(Status.INFO,"Test case Started");

		driver.findElement(By.id("tv_height")).click();
		test.log(Status.PASS, "Clicked on height");

		driver.findElement(By.id("ll_cancel")).click();
		test.log(Status.PASS, "Clicked on Cancel Button");
	}

	@Test(priority=5,groups={"personalProfile","all"})
	public void personalProfileSetWeight(){

		test =extent.createTest("PERSONAL PROFILE SET WIDTH","This Test Case will set the weight of patient.");
		test.log(Status.INFO,"Test case Started");

		driver.findElement(By.id("tv_weight")).click();
		test.log(Status.PASS, "Clicked on weight");

		MobileElement addweight= driver.findElement(By.id("wheel_weight"));
		ActionMethods.verticallyUp(addweight,200);
		test.log(Status.PASS, "Select value from list");

		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on save button");
	}

	@Test(priority=6,groups={"personalProfile","all"})
	public void verifyCancelButtonSetWidth(){	
		test =extent.createTest("WEIGHT CANCEL BUTTON","This Test Case will verify cancel Button is Functional.");
		test.log(Status.INFO,"Test case Started");

		driver.findElement(By.id("tv_weight")).click();
		test.log(Status.PASS, "Clicked on height");

		driver.findElement(By.id("ll_cancel")).click();
		test.log(Status.PASS, "Clicked on Cancel Button");
	}


	@Test(priority=7,groups={"personalProfile","all"})
	public void personalProfileSetBloodgroup(){	

		test =extent.createTest("PERSONAL PROFILE SET BlOOG GROUP ","This Test Case will set the Blood Group of patient.");
		test.log(Status.INFO,"Test case Started");

		driver.findElement(By.id("tv_blood_group")).click();
		test.log(Status.PASS, "Clicked on Blood Group");

		MobileElement addBloodGroup = driver.findElement(By.id("bottom_sheet_blood_group"));
		ActionMethods.verticallyUp(addBloodGroup,200); 
		test.log(Status.PASS, "Select value from list");

		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on save button");
	}

	@Test(priority=8,groups={"personalProfile","all"})
	public void verifyCancelButtonSetBloodgroup(){	
		test =extent.createTest("SET BLOOD GROUP CANCEL BUTTON","This Test Case will verify cancel Button is Functional.");
		test.log(Status.INFO,"Test case Started");

		driver.findElement(By.id("tv_blood_group")).click();
		test.log(Status.PASS, "Clicked on height");

		driver.findElement(By.id("ll_cancel")).click();
		test.log(Status.PASS, "Clicked on Cancel Button");
	}

	@Test(priority=9,groups={"personalProfile","all"})
	public void personalProfileSetMaritalStatus(){
		test =extent.createTest("PERSONAL PROFILE SET MARITAL STATUS ","This Test Case will set the Matrial status of patient.");
		test.log(Status.INFO,"Test case Started");

		driver.findElement(By.id("tv_marital_status")).click();
		test.log(Status.PASS, "Clicked on Blood Group");

		MobileElement wheelMarital = driver.findElement(By.id("wheel_marital"));
		ActionMethods.verticallyUp(wheelMarital,200); 
		test.log(Status.PASS, "Select value from list");

		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on save button");
	}

	@Test(priority=10,groups={"personalProfile","all"})
	public void verifyCancelButtonMaritalStatus(){	
		test =extent.createTest("MARITAL STATUS CANCEL BUTTON","This Test Case will verify cancel Button is Functional.");
		test.log(Status.INFO,"Test case Started");

		driver.findElement(By.id("tv_marital_status")).click();
		test.log(Status.PASS, "Clicked on height");

		driver.findElement(By.id("ll_cancel")).click();
		test.log(Status.PASS, "Clicked on Cancel Button");
	}


	@Test(priority=11,groups={"personalProfile","all"})
	public void personalProfileSetVisitDate(){	
		test =extent.createTest("PERSONAL PROFILE VISIT DATE","This Test Case will set the Last Visit date to doctor.");
		test.log(Status.INFO,"Test case Started");
		driver.findElement(By.id("tv_last_visit")).click();
		test.log(Status.PASS, "Clicked on Enter Date"); 


		driver.findElement(By.xpath("//android.widget.NumberPicker[@index=0]//android.widget.Button[@index=0]")).click();
		test.log(Status.PASS, "Select Year from List");

		driver.findElement(By.xpath("//android.widget.NumberPicker[@index=1]//android.widget.Button[@index=0]")).click();
		for(int x=0;x<3;x++) 
			driver.findElement(By.xpath("//android.widget.NumberPicker[@index=1]//android.widget.Button[@index=0]")).click();
		test.log(Status.PASS, "Select Month from list");

		for(int x=0;x<5;x++) 
			driver.findElement(By.xpath("//android.widget.NumberPicker[@index=2]//android.widget.Button[@index=0]")).click();
		test.log(Status.PASS, "Select Date from list");

		driver.findElement(By.id("android:id/button1")).click();
		test.log(Status.PASS, "Clicked on Ok Button");

	}

	@Test(priority=12,groups ={"personalProfile","all"},enabled=false)
	public void verifyCancelButtonLastRevist(){	
		test =extent.createTest("LAST VISIT DATE CANCEL BUTTON","This Test Case will verify cancel Button is Functional.");
		test.log(Status.INFO,"Test case Started");

		driver.findElement(By.id("tv_last_visit")).click();
		test.log(Status.PASS, "Clicked on Enter date");

		driver.findElement(By.id("button2")).click();
		test.log(Status.PASS, "Clicked on Cancel Button");
	}

	//HEALTH PROBLEM
	@Test(priority=13,groups ={"healthProblems","all"})
	public void healthProblems(){	
		test =extent.createTest("ADD HEALTH PROBLEMS DATA","This test case will Enter Health problems of patient");
		test.log(Status.INFO,"Test case Started");
		driver.findElement(By.id("health_problems_tab")).click();
		test.log(Status.PASS, "clicked Health Problem Tab");
	}


	@Test(priority=14,groups ={"healthProblems","all"})
	public void healthProblemList(){	
		MobileElement problem1 =driver.findElement(By.xpath("//android.widget.RelativeLayout[@index=2]//android.widget.RelativeLayout[@index=0]"));
		ActionMethods.tapOnElement(problem1);
		test.log(Status.PASS, "First problem is selected from list @index=0");
		MobileElement setSeverity = driver.findElement(By.id("wheel_severity"));
		ActionMethods.verticallyUp(setSeverity, 120);
		test.log(Status.PASS, "Severity of problem is set");
		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on Save button");


		MobileElement problem2 =driver.findElement(By.xpath("//android.widget.RelativeLayout[@index=2]//android.widget.RelativeLayout[@index=2]"));
		ActionMethods.tapOnElement(problem2);
		test.log(Status.PASS, "Third problem is selected from list @index=2");
		MobileElement setSeverity1 = driver.findElement(By.id("wheel_severity"));
		ActionMethods.verticallyUp(setSeverity1, 150);
		test.log(Status.PASS, "Severity of problem is set");
		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on Save button");

		MobileElement problem4 =driver.findElement(By.xpath("//android.widget.RelativeLayout[@index=2]//android.widget.RelativeLayout[@index=5]"));
		ActionMethods.tapOnElement(problem4);
		test.log(Status.PASS, "Sixth problem is selected from list @index=5");
		MobileElement setSeverity2 = driver.findElement(By.id("wheel_severity"));
		ActionMethods.verticallyUp(setSeverity2, 200);
		test.log(Status.PASS, "Severity of problem is set");
		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on Save button");

	}

	@Test(priority=15,groups={"allergies","all"})
	public void allergiesTab(){	
		test =extent.createTest("ADD ALLERGIES DATA","This test case will Enter Allergies data for patient");
		test.log(Status.INFO,"Test case Started");
		driver.findElement(By.id("allergies_tab")).click();
		test.log(Status.PASS, "Clicked on Allergies Tab");
	}

	@Test(priority=16,groups ={"allergies","all"})
	public void allergiesList(){
		MobileElement problem1 =driver.findElement(By.xpath("//android.widget.RelativeLayout[@index=4]//android.widget.RelativeLayout[@index=0]"));
		ActionMethods.tapOnElement(problem1);
		test.log(Status.PASS,"First Allergy is selected from list @index=0");
		MobileElement setSeverity = driver.findElement(By.id("wheel_severity"));
		ActionMethods.verticallyUp(setSeverity, 120);
		test.log(Status.PASS, "Severity of problem is set");
		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on Save button");


		MobileElement problem2 =driver.findElement(By.xpath("//android.widget.RelativeLayout[@index=4]//android.widget.RelativeLayout[@index=2]"));
		ActionMethods.tapOnElement(problem1);
		test.log(Status.PASS,"Third Allergy is selected from list @index=2");
		MobileElement setSeverity1 = driver.findElement(By.id("wheel_severity"));
		ActionMethods.verticallyUp(setSeverity1, 200);
		test.log(Status.PASS, "Severity of problem is set");
		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on Save button");

		MobileElement problem3 =driver.findElement(By.xpath("//android.widget.RelativeLayout[@index=4]//android.widget.RelativeLayout[@index=5]"));
		ActionMethods.tapOnElement(problem3);
		test.log(Status.PASS,"Sixth Allergy is selected from list @index=5");
		MobileElement setSeverity2= driver.findElement(By.id("wheel_severity"));
		ActionMethods.verticallyUp(setSeverity2, 120);
		test.log(Status.PASS, "Severity of problem is set");
		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on Save button");

	}


	@Test(priority=17,groups={"lifestyle","all"})
	public void lifeStyle(){
		test =extent.createTest("ADD LIFESTYLE DATA","This test case will add lifestyle data of patient");
		test.log(Status.INFO,"Test case Started");
		driver.findElement(By.id("lifestyle_tab")).click();
		test.log(Status.PASS, "clicked on Life style tab");
	}

	@Test(priority=18,groups={"lifestyle","all"})
	public void lifeStyleOptions(){
		driver.findElement(By.xpath("//android.widget.RelativeLayout[@index=1]//android.widget.RelativeLayout[@index=0]//android.widget.Switch[@index=1]")).click();
		test.log(Status.PASS, "First option is selected, if the toggle is ON it will turn if OFF and Vice versa");
		driver.findElement(By.xpath("//android.widget.RelativeLayout[@index=1]//android.widget.RelativeLayout[@index=1]//android.widget.Switch[@index=1]")).click();
		test.log(Status.PASS, "Second option is selected, if the toggle is ON it will turn if OFF and Vice versa");
	}

	@Test(priority=19,groups={"onGoingTreatment","all"})
	public void ongoingTreatment (){
		test =extent.createTest("ADD ONGOING TREATMENT DATA","This test case will treatment data.");
		test.log(Status.INFO,"Test case Started");
		driver.findElement(By.id("ongoing_treatment_tab")).click();
		test.log(Status.PASS, "clicked on  Ongoing Treatment tab");
	}

	@Test(priority=20,groups={"onGoingTreatment","all"},enabled=false)
	public void addOngoingTreatment(){
		driver.findElement(By.xpath("//android.widget.TextView[@text='Add New']")).click();
		test.log(Status.PASS, "clicked on  Add Button");

		MobileElement medicineName=driver.findElement(By.id("spMedicineNameValue"));
		medicineName.sendKeys("panadol");
		test.log(Status.PASS, "Panadol is entered, Matching record returned");
		MobileElement medicine= driver.findElement(By.xpath("//android.widget.TextView[@index=1]"));
		ActionMethods.WaitByvisibilityOfElement(medicine, 2);
		ActionMethods.tapOnElement(medicine);
		test.log(Status.PASS, "First item is selected from list of record");

		test.log(Status.PASS, "Medicine Name added ");

		driver.findElement(By.id("tvSinceDate")).click();
		test.log(Status.PASS, "Clicked on Date"); 


		driver.findElement(By.xpath("//android.widget.NumberPicker[@index=0]//android.widget.Button[@index=0]")).click();
		test.log(Status.PASS, "Select Year from List");

		driver.findElement(By.xpath("//android.widget.NumberPicker[@index=1]//android.widget.Button[@index=0]")).click();
		for(int x=0;x<3;x++) 
			driver.findElement(By.xpath("//android.widget.NumberPicker[@index=1]//android.widget.Button[@index=0]")).click();
		test.log(Status.PASS, "Select Month from list");

		for(int x=0;x<5;x++) 
			driver.findElement(By.xpath("//android.widget.NumberPicker[@index=2]//android.widget.Button[@index=0]")).click();
		test.log(Status.PASS, "Select Date from list");

		driver.findElement(By.id("android:id/button1")).click();
		test.log(Status.PASS, "Clicked on Ok Button");

		driver.findElement(By.id("ll_add")).click();
		test.log(Status.PASS, "Clicked on Save button");
	}

	@Test(priority=21,groups={"onGoingTreatment","all"},enabled=false)
	public void verifyCancelButtonAddTreatment(){

		test =extent.createTest("Verify Cancel Button on Adding Ongoing Treatment.");
		test.log(Status.INFO,"Test case Started");
		driver.findElement(By.id("ongoing_treatment_tab")).click();
		test.log(Status.INFO, "clicked on  Ongoing Treatment tab");

		driver.findElement(By.xpath("//android.widget.TextView[@text='Add New']")).click();
		test.log(Status.INFO, "clicked on  Add Button");

		driver.findElement(By.id("ll_cancel")).click();
		test.log(Status.INFO, "Clicked on Cancel button");

	}
	
	@Test(priority=23,groups={"onGoingTreatment","all"})
	void clickOnBack() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.id("iv_menu_back")).click();
		
	}
	
	
	

}

