package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

/**
 * Created by Javaria Naeem
 */
public class MenuReports extends BaseClass {
	ExtentTest test;
	Point p;
	WebDriverWait wait;
	
	@Test(priority=1, enabled=false)
	public void menu() throws InterruptedException 
	{
		test = extent.createTest("ClICK ON MENU","This test case will click on menu button");
		test.log(Status.INFO, "Test case Started");
		//Click on menu button
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_logo_menu")));
		MobileElement e1 =driver.findElement(By.id("iv_logo_menu"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Menu button clicked");
		Thread.sleep(5000);
	}
	
	@Test(priority=2, enabled=true)
	public void uploadreport() throws InterruptedException
	{    
		
		test = extent.createTest("UPLOAD REPORT FROM GALLERY","This test case will upload report from gallery");
		test.log(Status.INFO, "Test case Started");
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Reports']")));
		MobileElement e2 =driver.findElement(By.xpath("//android.widget.TextView[@text='Reports']"));
		ActionMethods.tapOnElement(e2);
		test.log(Status.PASS, "Reports button clicked");
		Thread.sleep(5000);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.LinearLayout[@index=2]")));
		MobileElement e3 =driver.findElement(By.xpath("//android.widget.LinearLayout[@index=2]"));
		ActionMethods.tapOnElement(e3);
		test.log(Status.PASS, "Gallary button clicked");
		//Select image from gallary
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.GridView//android.widget.FrameLayout[@index=0]")));
		driver.findElement(By.xpath("//android.widget.GridView//android.widget.FrameLayout[@index=0]")).click();
		test.log(Status.PASS, "Image selects from gallary");
		//upload report
		driver.findElement(By.id("iv_report_upload")).click();
		test.log(Status.PASS, "report uploaded");


	}

	@Test(priority=3, enabled=true)
	public void editreport() throws InterruptedException
	{    
		test = extent.createTest("EDIT UPLOADED REPORT","This test case will edit report");
		test.log(Status.INFO, "Test case Started");
		Thread.sleep(5000);


		MobileElement e4= driver.findElement(By.xpath("//android.view.ViewGroup[@index=0]//android.widget.ImageView[@index=1]"));
		ActionMethods.tapOnElement(e4);
		test.log(Status.PASS, "edit button clicked");
		Thread.sleep(3000);
		WebElement e5= driver.findElement(By.id("et_dialog_edit_text"));
		e5.clear();
		test.log(Status.PASS, "text cleared");
		e5.sendKeys("Report2");
		test.log(Status.PASS, "edited text entered");
		driver.findElement(By.id("btYes")).click();
		test.log(Status.PASS, "Report name edited successfully");
		Thread.sleep(3000);

	}
	@Test(priority=4, enabled=true)
	public void reportvisible() throws InterruptedException
	{    
		test = extent.createTest("CHECK REPORT VISIBILTY","This test case will enable/diable report visibility");
		test.log(Status.INFO, "Test case Started");
		Thread.sleep(5000);


		MobileElement e4= driver.findElement(By.xpath("//android.view.ViewGroup[@index=2]//android.widget.ImageView[@index=2]"));
		ActionMethods.tapOnElement(e4);
		test.log(Status.PASS, "Reports visivility off/on");
		Thread.sleep(3000);

	}
	@Test(priority=5 , enabled=true)
	public void deletereport() throws InterruptedException
	{    
		test = extent.createTest("DELETE REPORT","This test case will delete report");
		test.log(Status.INFO, "Test case Started");

		Thread.sleep(5000);
		MobileElement e4= driver.findElement(By.xpath("//android.view.ViewGroup[@index=2]//android.widget.ImageView[@index=3]"));
		ActionMethods.tapOnElement(e4);
		test.log(Status.PASS, "Reports selected , delete button clicked");
		Thread.sleep(3000);
		WebElement e5= driver.findElement(By.id("btYes"));
		e5.click();
		test.log(Status.PASS, "report deleted");
		Thread.sleep(3000);
		
		
	}
	@Test(priority=6 , enabled=true)
	public void baktomenu() throws InterruptedException 
	{
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_menu_back")));
		driver.findElement(By.id("iv_menu_back")).click();
		Thread.sleep(3000);
	}

}
