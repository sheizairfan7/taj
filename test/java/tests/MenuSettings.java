package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import utilities.ActionMethods;

/**
 * Created by Javaria Naeem
 */


public class MenuSettings extends BaseClass{
	
	ExtentTest test;
	Point p;
	WebDriverWait wait;
	
	@Test(priority=1 , enabled=true)
	public void settings() throws InterruptedException
	{
		test = extent.createTest("CHANGE SETTINGS","This test case will click on settings from menu");
		test.log(Status.INFO, "Test case Started");
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.RelativeLayout[@index=6]//android.widget.TextView[@index=1]")));
		driver.findElement(By.xpath("//android.widget.RelativeLayout[@index=6]//android.widget.TextView[@index=1]")).click();
	}
	@Test(priority=2 , enabled=true)
	public void selectdoctorgender() throws InterruptedException
	{test = extent.createTest("CHANGE DOCTOR GENDER","This test case will change doctor gender");
		test.log(Status.INFO, "Test case Started");
		driver.findElement(By.id("tv_gender_pref")).click();
		test.log(Status.PASS, "doctor gender preference clicked");
		driver.findElement(By.id("tv_gender_both")).click();
		test.log(Status.PASS, "'Any' button clicked");
		MobileElement e1=driver.findElement(By.id("tv_continue"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Continue button clicked");
	}
	@Test(priority=3 , enabled=true)
	public void selectdoctorlanguage() throws InterruptedException
	{
		test = extent.createTest("CHANGE DOCTOR language","This test case will change doctor LANGUAGE");
		test.log(Status.INFO, "Test case Started");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tv_lan_pref_select")));
		driver.findElement(By.id("tv_lan_pref_select")).click();
		test.log(Status.PASS, "choose doctor language clicked");
		driver.findElement(By.xpath("//androidx.recyclerview.widget.RecyclerView//android.widget.TextView[@index=3]")).click();
		test.log(Status.PASS, "language selected");
		MobileElement e2=driver.findElement(By.id("tv_continue"));
		ActionMethods.tapOnElement(e2);
		test.log(Status.PASS, "Consultation button clicked");

		Thread.sleep(5000);
	}
	@Test(priority=4 , enabled=true)
	public void changepin() throws InterruptedException
	{
		test = extent.createTest("CHANGE SECURITY PIN","This test case will change security pin");
		driver.findElement(By.id("tv_pin_edit")).click();
		driver.findElement(By.id("et_security_pin_digit_1")).sendKeys("1");
		driver.findElement(By.id("et_security_pin_digit_2")).sendKeys("1");
		driver.findElement(By.id("et_security_pin_digit_3")).sendKeys("1");
		driver.findElement(By.id("et_security_pin_digit_4")).sendKeys("1");
		test.log(Status.PASS, " Current Pin entered successfully");


		driver.findElement(By.id("et_security_new_pin_digit_1")).sendKeys("2");
		driver.findElement(By.id("et_security_new_pin_digit_2")).sendKeys("2");
		driver.findElement(By.id("et_security_new_pin_digit_3")).sendKeys("2");
		driver.findElement(By.id("et_security_new_pin_digit_4")).sendKeys("2");
		test.log(Status.PASS, " New Pin entered successfully");


		driver.findElement(By.id("et_security_confirm_pin_digit_1")).sendKeys("2");
		driver.findElement(By.id("et_security_confirm_pin_digit_2")).sendKeys("2");
		driver.findElement(By.id("et_security_confirm_pin_digit_3")).sendKeys("2");
		driver.findElement(By.id("et_security_confirm_pin_digit_4")).sendKeys("2");
		test.log(Status.PASS, "Confirm Pin entered successfully");
		driver.findElement(By.id("bt_security_pin")).click();
		test.log(Status.PASS, " Continue button clicked");
		Thread.sleep(5000);

	}
	@Test(priority=5)
	public void backtomenu() throws InterruptedException 
	{
		test = extent.createTest("ClICK ON MENU","This test case will click on menu button");
		test.log(Status.INFO, "Test case Started");
		//Click on menu button
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_logo_menu")));
		MobileElement e1 =driver.findElement(By.id("iv_logo_menu"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Menu button is clicked");
		Thread.sleep(5000);
	}

}
