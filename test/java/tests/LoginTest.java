package tests;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.TouchAction;

/**
 * Created by Shanza Irfan Qureshi
 */
public class LoginTest extends BaseClass{

	public ExtentTest test;

	@Test(priority=0)
	public void appLanguageScreen() {

		test =extent.createTest("SELECT APPLICATION LANGUAGE","This test case will load language screen and select language");
		test.log(Status.INFO,"Test case Started");	

		WebDriverWait wait= new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_eng")));	

		driver.findElement(By.id("iv_eng")).click();
		test.log(Status.PASS, "English Language is selected");

		driver.findElement(By.id("linearLayout3")).click();
		test.log(Status.PASS, "Continue Button is clicked");	

	}

	@Test(priority=1)
	public void enterMobileNumber() {

		test =extent.createTest("ENTER MOBILE NUMBER","This test case will Enter Mobile Number");
		test.log(Status.INFO,"Test case Started");	

		WebDriverWait wait= new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("et_mobile_num_")));	

		driver.findElement(By.id("et_mobile_num_")).sendKeys("89988998");
		test.log(Status.PASS, "Mobile Number is entered");

		driver.findElement(By.id("linearLayout3")).click();
		test.log(Status.PASS, "Continue Button is clicked");	

	}

	@Test(priority=2)
	public void enterVerificationCode() {

		test =extent.createTest("ENTER VERIFICATION CODE","This test case will Enter 010101 Verification code");
		test.log(Status.INFO,"Test case Started");	

		driver.findElement(By.id("et_verification_code")).sendKeys("0");
		driver.findElement(By.id("et_verification_code2")).sendKeys("1");
		driver.findElement(By.id("et_verification_code3")).sendKeys("0");
		driver.findElement(By.id("et_verification_code4")).sendKeys("1");
		driver.findElement(By.id("et_verification_code5")).sendKeys("0");
		driver.findElement(By.id("et_verification_code6")).sendKeys("1");

		test.log(Status.PASS, "OPTP Entered");	
	}

	@Test(priority=3)
	public void enterPin() {

		String text =driver.findElement(By.id("tv_title_security")).getText();
		SoftAssert assertion= new SoftAssert();
		assertion.assertTrue(text.equalsIgnoreCase("Enter PIN"));

		try{
			test =extent.createTest("ENTER PIN","This test case will Enter pin 1111");
			test.log(Status.INFO,"Test case Started");	

			driver.findElement(By.id("et_security_pin_digit_1")).sendKeys("1");
			driver.findElement(By.id("et_security_pin_digit_2")).sendKeys("1");
			driver.findElement(By.id("et_security_pin_digit_3")).sendKeys("1");
			driver.findElement(By.id("et_security_pin_digit_4")).sendKeys("1");
			test.log(Status.PASS,"PIN Entered");	
		}
		catch(AssertionError e) {

			test.log(Status.FAIL, e);	
		}
	}

	@Test(priority=4)
	public void onHome() {

		test =extent.createTest("VERIFY HOME SCREEN","This test case will Verify that user is on Home Screen");
		String element=driver.findElement(By.id("view_empty")).getText();
		String text="L I  F  E  S  T  Y  L  E      S  C  O  R  E";
		SoftAssert assertion = new SoftAssert();
		assertion.assertTrue(text.equalsIgnoreCase(element));	
		try{
			test.log(Status.PASS,"User is on Home Screen");	
			assertion.assertAll();
		}
		catch(AssertionError e) {
			test.log(Status.FAIL, e);	
		}
	}
}
