package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import utilities.ActionMethods;
import io.appium.java_client.touch.WaitOptions;
import java.time.Duration;

/**
 * Created by Javaria Naeem
 */

public class HealthscoreFitness extends ToastVeification {


	ExtentTest test;
	Point p;
	WebDriverWait wait;

	@Test(priority=1)
	void lifestylescore() throws InterruptedException
	{
		test = extent.createTest("ClICK ON LIFESTYLESCORE","This test case will click on lifestyle score");
		test.log(Status.INFO, "Test Case Started");
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("health_score")));
		driver.findElement(By.id("health_score")).click();
		test.log(Status.PASS, "Heathscore button is clicked");	
		Thread.sleep(5000);	

	}
//just for testing
	@Test(priority=1)
	void lifestylescore1() throws InterruptedException
	{
		test = extent.createTest("ClICK ON LIFESTYLESCORE","This test case will click on lifestyle score");
		test.log(Status.INFO, "Test Case Started");
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("health_score")));
		driver.findElement(By.id("health_score")).click();
		test.log(Status.PASS, "Heathscore button is clicked");	
		Thread.sleep(5000);	

	}
	@Test(priority=2)
	void fitnessbuttonclick() throws InterruptedException
	{
		test = extent.createTest("ClICK ON FITNESS BUTTON","This test case will click on lifestyle score");
		test.log(Status.INFO, "Started");
		driver.findElement(By.id("fitnes_box")).click();
		test.log(Status.PASS, "fitness box is clicked");

	}
	@Test(priority=3, enabled=true)
	void addfitnessscore() throws InterruptedException
	{
		test = extent.createTest("ADD FITNESS SCORE","This test case will add fitness score");
		test.log(Status.INFO, "Started");
		MobileElement e1= driver.findElement(By.id("risk_add"));
		ActionMethods.tapOnElement(e1);
		test.log(Status.PASS, "Add "+" button is clicked");
		driver.findElement(By.id("iv_walk_activty")).click();
		test.log(Status.PASS, "Walk button is clicked");
		driver.findElement(By.id("iv_level_medium")).click();
		test.log(Status.PASS, "medium button is clicked");
	}

	@Test(priority=4)
	void adddistance() throws InterruptedException
	{
		MobileElement e2=driver.findElement(By.id("pluse_add_distance"));
		ActionMethods.tapOnElement(e2);
		test.log(Status.PASS, "Add distance button is clicked");
	}
	
	@Test(priority=5)
	void addkilometers() throws InterruptedException
	{
		
		MobileElement kilometers= driver.findElement(By.id("wheel_distances_km"));
		ActionMethods.verticallyDown(kilometers, 150);
		test.log(Status.PASS, "kilometres list is scrolled");
		Thread.sleep(2000);
	}
	
	@Test(priority=6, enabled=true)
	void addmeters() throws InterruptedException
	{

		MobileElement meters= driver.findElement(By.id("wheel_distance_meters"));
		ActionMethods.verticallyDown(meters, 150);
		test.log(Status.PASS, "meters list is scrolled");

		Thread.sleep(2000);
	}
	
	@Test(priority=7, enabled=true)
	void submitdistance() throws InterruptedException
	{
		driver.findElement(By.id("btn_done")).click();
		test.log(Status.PASS, "distance is added");
		Thread.sleep(5000);
	}

	@Test(priority=8 , enabled=true)
	void addTime() throws InterruptedException
	{
		MobileElement e3=driver.findElement(By.id("pluse_add_time"));
		
        ActionMethods.tapOnElement(e3);
		test.log(Status.PASS, "Add time button clicked successfully");

	}

	@Test(priority=9 , enabled=true)
	void addhours() throws InterruptedException
	{

		MobileElement hours= driver.findElement(By.id("wheel_time_hours"));
		ActionMethods.verticallyDown(hours, 150);
		test.log(Status.PASS, "hours list is scrolled");

		Thread.sleep(2000);
	}
	
	@Test(priority=10, enabled=true)
	void addminutes() throws InterruptedException
	{
		MobileElement minutes= driver.findElement(By.id("wheel_time_minutes"));
		ActionMethods.verticallyDown(minutes, 150);
		test.log(Status.PASS, "minutes list is scrolled");
		Thread.sleep(2000);
	}
	@Test(priority=11, enabled=true)
	void submitime() throws InterruptedException
	{
		driver.findElement(By.id("btn_done")).click();
		test.log(Status.PASS, "distance is added");
		Thread.sleep(5000);
	}
	@Test(priority=12, enabled=true)
	void calculatefitnessscore() throws InterruptedException
	{
		driver.findElement(By.id("bt_calculate")).click();	
		test.log(Status.PASS, "Fitness score is updated ");
		Thread.sleep(3000);
	}
	@Test(priority=13, enabled=true)
	void deletefitnesscore() throws InterruptedException
	{
		test = extent.createTest("DELETE FITNESS SCORE CLICK","This test case will click on delete button");
		test.log(Status.INFO, "Started");
		driver.findElement(By.id("iv_delete_records")).click();
		test.log(Status.PASS, "Delete button is clicked");
	}
	@Test(priority=14, enabled=true)
	void resetfitnessscore() throws InterruptedException
	{
		test = extent.createTest("RESET FITRNESS SCORE","This test case will reset fitness score");
		test.log(Status.INFO, "Test Case Started");
		driver.findElement(By.id("btn_delete_all")).click();
		test.log(Status.PASS, "Reset All button is clicked ");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		test.log(Status.PASS, "Fitness score is deleted ");	
		Thread.sleep(3000);
		
	}
	@Test(priority=15, enabled=true)
	void checkalreadydeletedshapedata() throws InterruptedException
	{
		test = extent.createTest("RESET FITRNESS SCORE AGAIN","This test case will reset fitness score again to verify toast message");
		test.log(Status.INFO, "Started");
		deletefitnesscore();
		resetfitnessscore();
		test.log(Status.INFO, "Screen shot taken to verify toast if data is not present");
		screenshot();
		test.log(Status.PASS, "Screenshot taken successfully");
		String Text= OCR(scrPath);
		System.out.println(Text);
		SoftAssert softassert = new SoftAssert();
		softassert.assertEquals(Text.contains("\"Fitness history is not available, Please add your Fitness data first.\";"), Text.equals("\"Fitness history is not available, Please add your Fitness data first.\";"));
		try {
			test.log(Status.PASS, "Toast Verified . there is no data please add data and then tries to delete");
			WebDriverWait wait=new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("iv_menu_back")));
			driver.findElement(By.id("iv_menu_back")).click();
			Thread.sleep(5000);
		}
		catch(AssertionError e)
		{
			test.log(Status.FAIL, e);
		}


	}
	//Test Scenario 5 : Verify that user can delete shape data individually


	@Test(priority=16 , enabled=true)
	void deletefitnessscoreindividually() throws InterruptedException
	{
		addfitnessscore();
		adddistance();
		addkilometers();
		addmeters();
		submitdistance();
		addTime();
		addhours();
		addminutes();
		submitime();
		calculatefitnessscore();
		test = extent.createTest("DELETE FITNESS SCORE","This test case will delete fitness score");
		test.log(Status.INFO, "Test Case Started");
		driver.findElement(By.id("iv_delete_records")).click();
		driver.findElement(By.id("img_delete_solid")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']")).click();
		Thread.sleep(5000);
		test.log(Status.PASS, "Fitness score is deleted");	
		driver.findElement(By.id("iv_menu_back")).click();
		Thread.sleep(3000);
	}
	
	@Test(priority=17 , enabled=true)
	void homescreen() throws InterruptedException
	{
		test = extent.createTest("BACK TO HOME SCREEN","This test case will navigate to home screen");
		driver.findElement(By.id("iv_home_logo")).click();
		Thread.sleep(5000);
	}

}
