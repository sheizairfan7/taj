package dataProvider;
/**
 * Created by Shanza irfan Qureshi
 */
public class TestData {

	public static long SELENIUM_RESULTS_MIN;
	//    private static String getTestData(String scenario, String feature, String testcase)  {
	//		String testDataString = null; 
	//    	try {
	//    		testDataString= readExcelData("Testdata", feature, scenario).get(testcase).toString();
	//		} catch (IOException e) {
	//			e.printStackTrace();
	//		}
	//		return testDataString;
	//	}
	public interface DataValue {
		public final static String MOBILE_NUMBER = "89988998";
		public final static String FAILED_STATUS = "failed";
		public final static String PASS_STATUS = "PASS";
		public final static String FAIL_STATUS = "FAIL";
	}
	public interface Email {
		public final static String EMAIL_RESULT_SUBJECT = "Test Automation Results";
		public final static String EMAIL_SEND_FROM_LABEL = "Automation Result";
		public final static String FAILED_STATUS = "failed";
		public final static String PASS_STATUS = "PASS";
		public final static String FAIL_STATUS = "FAIL";
	}

	/*
	 * All Information Messages are define here
	 */
	public interface InformationMessage {
		public final static String INTERNET_ERROR = "";
		public final static String API_FAIL = "API fail to process the request";
		public final static String ELEMENT_NOT_FOUND_EXCEPTION = " was not visible on UI for some reason, so application was not able to interact with it. ";
		public final static String INVALID_MOBILE_NUMBER = "Please enter valid mobile number";
	}

}
