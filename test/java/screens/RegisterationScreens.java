//package screens;
//
//import static utilities.ExcelFileManager.readFromCell;
//
//import java.io.IOException;
//
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.PageFactory;
//
//import utilities.PropertyReader;
//import utilities.UtilityMethods;
//import io.appium.java_client.AppiumDriver;
//import io.appium.java_client.MobileElement;
//import io.appium.java_client.android.AndroidDriver;
//import tests.BaseClass;
//
//public class RegisterationScreens extends BaseClass {
//	
//
//    public static String fileName =(new PropertyReader().readProperty("appConfigFile"));
//    public static String ConfigfileName;
//
//    static {
//        try {
//            ConfigfileName = readFromCell(fileName,"SetUp",1,"fileName");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }	
//    
//    
//    public RegisterationScreens(AppiumDriver<MobileElement>  driver) throws Exception {
//        PageFactory.initElements(driver,this);
//    }
//    
//    public void Clickelemet(String Sheet, String rowMatch) throws IOException, InterruptedException {
// 
//        WebElement element = UtilityMethods.getElementById(readFromCell(ConfigfileName,Sheet,1,rowMatch));
//        System.out.println("Element found "+element.getText());
//      
//        try{
//            element.click();
//            System.out.println("Clicked on Element "+rowMatch);
//
//        }catch (Exception ex){
//            Actions actions = new Actions(driver);
//            actions.moveToElement(element).click().perform();
//            System.out.println("Clicked on Element by Action "+rowMatch);
//        }
//    }
//	
//}
