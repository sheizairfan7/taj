package utilities;

import java.time.Duration;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import tests.BaseClass;

public class ActionMethods extends BaseClass {

	public static Point point;

	public static void tapOnElement(MobileElement element)
	{
		point=element.getLocation();
		TouchAction<?> actions = new TouchAction(driver);
		actions.tap(PointOption.point(point.x, point.y)).perform(); 
	}

	//list value will be increased
	public static void verticallyUp(MobileElement element,int y) {
		point = element.getLocation();
		TouchAction<?> actions = new TouchAction(driver);
		actions.press(PointOption.point(point.x,point.y))
		.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
		.moveTo(PointOption.point(point.x,point.y-y)).release().perform();
	}

	//list value will be decreased
	public static void verticallyDown(MobileElement element,int y) {
		point = element.getLocation();
		TouchAction<?> actions = new TouchAction(driver);
		actions.press(PointOption.point(point.x,point.y))
		.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
		.moveTo(PointOption.point(point.x,point.y+y)).release().perform();
	}

	public static void moveForward(MobileElement element,int x) {
		point = element.getLocation();
		TouchAction<?> actions = new TouchAction(driver);
		actions.press(PointOption.point(point.x,point.y))
		.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
		.moveTo(PointOption.point(point.x+x,point.y)).release().perform();
	}

	public static void movebackwards(MobileElement element,int x) {
		point = element.getLocation();
		TouchAction<?> actions = new TouchAction(driver);
		actions.press(PointOption.point(point.x,point.y))
		.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
		.moveTo(PointOption.point(point.x-x,point.y)).release().perform();
	}
	public static void WaitByvisibilityOfElement(MobileElement element,int WaitInSecond){
		WebDriverWait wait= new WebDriverWait(driver,WaitInSecond);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public static void horizontalscroll(MobileElement e1,MobileElement e2)  
	{
		int midOfY =e1.getLocation().y +(e1.getSize().height/2);
		int fromXLocation=e1.getLocation().x;
		int toXLocation=e2.getLocation().x;

		TouchAction <?> action =new TouchAction(driver);
		action.press(PointOption.point(fromXLocation, midOfY))
		.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
		.moveTo(PointOption.point(toXLocation, midOfY))
		.release()
		.perform();
	}
	
//  x co ordinates remains the same but y co ordinates changes simultaneously.
	public static void verticalswipe_TtoB()
	{
		Dimension size	=driver.manage().window().getSize();
		int width=size.width;
		int height=size.height;				
		int middleOfX=width/2;
		int startYCoordinate= (int)(height*.7);
		int endYCoordinate= (int)(height*.2);
		TouchAction <?> action1 =new TouchAction(driver);			
		action1.press(PointOption.point(middleOfX, startYCoordinate))
		.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
		.moveTo(PointOption.point(middleOfX, endYCoordinate)).release().perform();
	}
}
